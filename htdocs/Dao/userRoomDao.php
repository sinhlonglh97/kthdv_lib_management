<?php
class userRoomDao {
    public function __construct() {
    }
    public function getuserRoomList(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userRoomServices/userRoom/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $userRoomList = curl_exec($ch);
        curl_close($ch);
        $userRoomList = json_decode($userRoomList);
        return $userRoomList;
    }
    public function getuserRoomWaitList(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userRoomServices/userRoomWait/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $userRoomList = curl_exec($ch);
        curl_close($ch);
        $userRoomList = json_decode($userRoomList);
        return $userRoomList;
    }

    public function  updateuserRoom($id,$father,$status,$description){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userRoomServices/update/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'father'=>$father,'status'=>$status,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function  insertuserRoom($id,$father,$status,$description){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userRoomServices/insert/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'father'=>$father,'status'=>$status,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function  returnRoom($userID,$roomID){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userRoomServices/returnRoom/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('userID'=>$userID,'roomID'=>$roomID);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
?>

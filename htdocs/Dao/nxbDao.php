<?php
class nxbDao {
    public function __construct() {
    }
    public function getnxbList(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/NxbServices/nxb/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $nxbList = curl_exec($ch);
        curl_close($ch);
        $nxbList = json_decode($nxbList);
        return $nxbList;
    }

    public function  updatenxb($id,$name,$status,$description){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/NxbServices/update/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'name'=>$name,'status'=>$status,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function  insertnxb($id,$name,$status,$description){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/NxbServices/insert/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'name'=>$name,'status'=>$status,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
?>

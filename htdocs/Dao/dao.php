<?php
class Dao {
    public function __construct() {
    }
    public function getAuthorList() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/author/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $authorList = curl_exec($ch);

        echo "<script>console.log( $authorList );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $authorList = json_decode($authorList);
        return $authorList;
    }
    public function addAuthor($name, $description) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/addAuthor/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('name' => $name,'description' => $description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function editAuthor($id ,$name, $description) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/updateAuthor/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'name' => $name,'description' => $description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function getLanguageList() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/getLanguages/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $languageList = curl_exec($ch);

        echo "<script>console.log( $languageList );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $languageList = json_decode($languageList);
        return $languageList;
    }

    public function addLanguage($name, $description) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/addLanguage/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('name' => $name,'description' => $description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addLanguage');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function editLanguage($id ,$name, $description) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/updateLanguage/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'name' => $name,'description' => $description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('editLanguage');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }


    public function getAuthor($id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/editAuthor/".$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $author = curl_exec($ch);
        $data = array('id' => $id);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        echo "<script>console.log( $author );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $author = json_decode($author);
        return $author;
    }

    public function getLanguage($id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/editLanguage/".$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $language = curl_exec($ch);
        $data = array('id' => $id);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        echo "<script>console.log( $language );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $language = json_decode($language);
        return $language;
    }

    public function login($username,$password)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/sign-in-secure-v2/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $data = array('username'=>$username,'password'=>$password);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        $output = curl_exec($ch);
        echo "<script>console.log( $output );</script>";
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($output);
        return $output;
    }


    public function getRoomCategoryList() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/getRoomCategories/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $list = curl_exec($ch);

        echo "<script>console.log( $list );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $list = json_decode($list);
        return $list;
    }


    public function addRoomCategory($name, $description) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/addRoomCategory/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('name' => $name,'description' => $description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function getRoomCategory($id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/editRoomCategory/".$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $room_category = curl_exec($ch);
        $data = array('id' => $id);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        echo "<script>console.log( $room_category );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $room_category = json_decode($room_category);
        return $room_category;
    }

    public function editRoomCategory($id ,$name, $description) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/updateRoomCategory/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'name' => $name,'description' => $description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function addRoom($name, $description, $roomC ,$status) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/addRoom/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('name' => $name,'description' => $description,'roomC'=>$roomC,'status'=>$status);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function addFaculty($name, $description) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/addFaculty/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('name' => $name,'description' => $description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function addClass($name,$fID) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/addClass/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('name' => $name, 'fID'=> $fID);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function getFaculty($id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/editFaculty/".$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $r = curl_exec($ch);
        $data = array('id' => $id);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        echo "<script>console.log( $r );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $r = json_decode($r);
        return $r;
    }

    public function getClass($id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/editClass/".$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $r = curl_exec($ch);
        $data = array('id' => $id);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        echo "<script>console.log( $r );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $r = json_decode($r);
        return $r;
    }



    public function getRoom($id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/editRoom/".$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $r = curl_exec($ch);
        $data = array('id' => $id);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        echo "<script>console.log( $r );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $r = json_decode($r);
        return $r;
    }

    public function editFaculty($id ,$name, $description) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/updateFaculty/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'name' => $name,'description' => $description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function getFacultyList() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/getFaculties/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $list = curl_exec($ch);

        echo "<script>console.log( $list );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $list = json_decode($list);
        return $list;
    }

    public function getClassList() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/getClasses/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $list = curl_exec($ch);

        echo "<script>console.log( $list );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $list = json_decode($list);
        return $list;
    }

    public function getRoomList() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/getRooms/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $list = curl_exec($ch);

        echo "<script>console.log( $list );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $list = json_decode($list);
        return $list;
    }
    public function editClass($name, $fID) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/updateClass/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('name' => $name,'fID' => $fID);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('editLanguage');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function editRoom($id,$name,$roomC ,$description, $status){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/updateRoom/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'name' => $name,'cID' => $roomC,'description'=>$description, 'status'=>$status);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('editLanguage');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }



}
?>
<script src="./assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="./assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Argon JS -->
<script src="./assets/js/argon.js?v=1.0.0"></script>

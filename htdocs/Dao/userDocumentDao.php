<?php
class userDocumentDao {
    public function __construct() {
    }
    public function getuserDocumentList(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userDocumentServices/userDocument/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $userDocumentList = curl_exec($ch);
        curl_close($ch);
        $userDocumentList = json_decode($userDocumentList);
        return $userDocumentList;
    }

    public function getuserDocumentWaitList(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userDocumentServices/userDocumentWait/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $userDocumentList = curl_exec($ch);
        curl_close($ch);
        $userDocumentList = json_decode($userDocumentList);
        return $userDocumentList;
    }

    public function  updateuserDocument($id,$father,$status,$description){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userDocumentServices/update/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'father'=>$father,'status'=>$status,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function  insertuserDocument($id,$father,$status,$description){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userDocumentServices/insert/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'father'=>$father,'status'=>$status,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function  returnDocument($userID,$documentID,$quantity){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/userDocumentServices/returnDocument/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('userID'=>$userID,'documentID'=>$documentID,'quantity'=>$quantity);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
?>

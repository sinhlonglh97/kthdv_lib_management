<?php
class documentDao {
    public function __construct() {
    }
    public function getdocumentList(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/DocumentServices/document/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $documentList = curl_exec($ch);
        curl_close($ch);
        echo "<script>console.log($documentList)</script>";
        $documentList = json_decode($documentList);

        return $documentList;
    }

    public function  updatedocument($id,$father,$status,$description){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/DocumentServices/update/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=>$id,'father'=>$father,'status'=>$status,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function  insertdocument($documentName,$documentQuantity,$documentPrice,
                                    $documentCountTB,$documentSizePage,$documentNumberPH,$datePH,
                                    $dateUpdate,$documentAuthor,$documentCategory,$documentNxb,
                                    $documentProducer,$documentLanguage,$documentPlace,$documentContent){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/DocumentServices/insert/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('documentName'=>$documentName,'documentQuantity'=>$documentQuantity,'documentPrice'=>$documentPrice,
                        'documentCountTB'=>$documentCountTB,'documentSizePage'=>$documentSizePage,'documentNumberPH'=>$documentNumberPH,'datePH'=>$datePH,
            'dateUpdate'=>$dateUpdate,'documentAuthor'=>$documentAuthor,'documentCategory'=>$documentCategory,'documentNxb'=>$documentNxb,
            'documentProducer'=>$documentProducer,'documentLanguage'=>$documentLanguage,'documentPlace'=>$documentPlace,'documentContent'=>$documentContent);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function  borrow($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/DocumentServices/borrow/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('bookID'=>$bookID,'userID'=>$userID,'startDate'=>$startDate,
            'endDate'=>$endDate,'description'=>$description,'quantity'=>$quantity,'quantityUpdate'=>$quantityUpdate);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function  accept($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/DocumentServices/accept/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('bookID'=>$bookID,'userID'=>$userID,'startDate'=>$startDate,
            'endDate'=>$endDate,'description'=>$description,'quantity'=>$quantity,'quantityUpdate'=>$quantityUpdate);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function  deny($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/DocumentServices/deny/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('bookID'=>$bookID,'userID'=>$userID,'startDate'=>$startDate,
            'endDate'=>$endDate,'description'=>$description,'quantity'=>$quantity,'quantityUpdate'=>$quantityUpdate);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function search($value,$option){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/DocumentServices/search/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('value'=>$value,'option'=>$option);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        echo "<script>console.log($result)</script>";
        curl_close($ch);
        $result = json_decode($result);
        return $result;
    }
}
?>
<script src="./assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="./assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Argon JS -->
<script src="./assets/js/argon.js?v=1.0.0"></script>
<?php
class userDao {
    public function __construct() {
    }
    public function addUser($id,$name,$faculty ,$class, $username,$password,$email,$role) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/addUser/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=> $id,'name'=>$name,'faculty'=>$faculty ,'class'=>$class, 'username'=>$username,
            'password'=>$password,'email'=>$email,'role'=>$role);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";

        $info1 = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function editUser($id,$name,$email,$phone ,$birthday, $address) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/editUser/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('id'=> $id,'name'=>$name,'email'=>$email,'phone'=>$phone ,'birthday'=>$birthday, 'address'=>$address);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
//        echo "<script>console.log('addAuthor');</script>";
        $info1 = curl_getinfo($ch);
        $result = json_decode($result);
        curl_close($ch);
        return $result;
    }

    public function getUsers() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/BookServices/getUsers/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $list = curl_exec($ch);

        echo "<script>console.log( $list );</script>";
        $info1 = curl_getinfo($ch);
        curl_close($ch);
        $list = json_decode($list);
        return $list;
    }
}
?>

<?php
class overviewDao {
    public function __construct() {
    }
    public function getoverviewList(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/OverViewServices/OverView/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $overviewList = curl_exec($ch);
        curl_close($ch);
        $overviewList = json_decode($overviewList);
        return $overviewList;
    }
}
?>

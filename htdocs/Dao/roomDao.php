<?php
class roomDao
{
    public function __construct() {
    }
    public function getRoomEmpty(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/RoomServices/getRoomEmpty/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

        $documentList = curl_exec($ch);
        curl_close($ch);
        echo "<script>console.log($documentList)</script>";
        $documentList = json_decode($documentList);

        return $documentList;
    }
    public function search($value,$option){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/RoomServices/search/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('value'=>$value,'option'=>$option);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        echo "<script>console.log($result)</script>";
        curl_close($ch);
        $result = json_decode($result);
        return $result;
    }
    public function borrow($roomID,$userID,$date,$timeStart,$timeEnd,$description){
        echo "<script>console.log($roomID)</script>";
        echo "<script>console.log($userID)</script>";
        echo "<script>console.log($date)</script>";
        echo "<script>console.log($timeStart)</script>";
        echo "<script>console.log($timeEnd)</script>";
        echo "<script>console.log($description)</script>";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/RoomServices/borrow/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('roomID'=>$roomID,'userID'=>$userID,'date'=>$date,'timeStart'=>$timeStart,'timeEnd'=>$timeEnd,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        return $result;
    }
    public function accept($roomID,$userID,$date,$timeStart,$timeEnd,$description){
        echo "<script>console.log($roomID)</script>";
        echo "<script>console.log($userID)</script>";
        echo "<script>console.log($date)</script>";
        echo "<script>console.log($timeStart)</script>";
        echo "<script>console.log($timeEnd)</script>";
        echo "<script>console.log($description)</script>";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/RoomServices/accept/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('roomID'=>$roomID,'userID'=>$userID,'date'=>$date,'timeStart'=>$timeStart,'timeEnd'=>$timeEnd,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        return $result;
    }
    public function deny($roomID,$userID,$date,$timeStart,$timeEnd,$description){
        echo "<script>console.log($roomID)</script>";
        echo "<script>console.log($userID)</script>";
        echo "<script>console.log($date)</script>";
        echo "<script>console.log($timeStart)</script>";
        echo "<script>console.log($timeEnd)</script>";
        echo "<script>console.log($description)</script>";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "18.219.218.11:6969/LibraryServices/rest/RoomServices/deny/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // In Java: @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
        $data = array('roomID'=>$roomID,'userID'=>$userID,'date'=>$date,'timeStart'=>$timeStart,'timeEnd'=>$timeEnd,'description'=>$description);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        return $result;
    }
}
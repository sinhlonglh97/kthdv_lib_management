<?php
session_start();
$role = $_SESSION['role'];
include_once("Controller/documentController.php");
$controller = new documentController();
$documentList = $controller->document();
if(isset($_POST['documentID'])){
    $borrowID = $_POST['documentID'];
}

if(isset($_POST['huy'])){
    header('Location: document.php');
}else
    if(isset($_POST['luu'])&&(isset($_POST['soluong']))){
        include_once("Controller/documentController.php");
        $controller = new documentController();
        $bookID = $_POST['bookID'];
        $quantity = $_POST['soluong'];
        $quantityUpdate = 0;
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];
        $description = $_POST['note'];
        $userID = $_SESSION['userID'];
        $result = $controller->borrow($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate);
        header('Location: document.php');
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Argon Dashboard - Free Dashboard for Bootstrap 4</title>
    <!-- Favicon -->
    <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="./assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="./assets/css/argon.css?v=1.0.0" rel="stylesheet">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="./assets/js/argon.js?v=1.0.0"></script>
</head>

<body>
<!-- Sidenav -->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="../index.html">
            <img src="./assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="./assets/img/theme/team-1-800x800.jpg">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>My profile</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>Settings</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>Activity</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>Support</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="logout.php" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="../index.html">
                            <img src="./assets/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">
                        <i class="ni ni-tv-2 text-primary"></i> Trang chủ
                    </a>
                </li>
                <?php
                if($role!='student' && $role != 'collaborator'){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link " href="category.php">
                            <i class="ni ni-planet text-blue"></i> Loại tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producer.php">
                            <i class="ni ni-pin-3 text-orange"></i> Nhà sản xuất
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="nxb.php">
                            <i class="ni ni-single-02 text-yellow"></i> Nhà xuất bản
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listAuthor.php">
                            <i class="ni ni-single-02 text-yellow"></i> Danh sách tác giả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listLanguage.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Danh sách ngôn ngữ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listRoom.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Danh sách phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listRoomCategory.php">
                            <i class="ni ni-key-25 text-info"></i> Danh sách loại phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="place.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Vị trí tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listClass.php">
                            <i class="ni ni-planet text-blue"></i> Danh sách lớp
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listFaculty.php">
                            <i class="ni ni-planet text-blue"></i> Danh sách khoa
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="document.php">
                            <i class="ni ni-key-25 text-info"></i> Danh sách tài liệu
                        </a>
                    </li>
                    <?php
                }
                ?>

                <li class="nav-item">
                    <a class="nav-link" href="searchDocument.php">
                        <i class="ni ni-key-25 text-info"></i> Tra cứu - tìm tài liệu
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="searchRoom.php">
                        <i class="ni ni-key-25 text-info"></i> Đặt phòng
                    </a>
                </li>
                <?php
                if($role!='student' && $role != 'staff'){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="userDocument.php">
                            <i class="ni ni-circle-08 text-pink"></i> Danh sách mượn tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userRoom.php">
                            <i class="ni ni-circle-08 text-pink"></i> Danh sách đặt phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userDocumentWait.php">
                            <i class="ni ni-circle-08 text-pink"></i> Duyệt mượn tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userRoomWait.php">
                            <i class="ni ni-circle-08 text-pink"></i> Duyệt đặt phòng
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
        </div>
    </div>
</nav>
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../index.html">quản lý vị trí</a>
            <!-- Form -->
            <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input class="form-control" placeholder="Search" type="text">
                    </div>
                </div>
            </form>
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="./assets/img/theme/team-4-800x800.jpg">
                </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold"><?=$_SESSION['username']?></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-settings-gear-65"></i>
                            <span>Settings</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-calendar-grid-58"></i>
                            <span>Activity</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-support-16"></i>
                            <span>Support</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="logout.php" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">

    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <!-- Table -->
        <div class="row">
            <div class="col-xl-6 order-xl-2">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Thông tin sách</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0 pt-md-4">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Tên tài liệu <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><?=$documentList[$borrowID]->{'documentName'}?></div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Tác giả <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><a href=""><?=$documentList[$borrowID]->{'documentAuthorName'}?></a></div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Nhà phát hành <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><a href=""><?=$documentList[$borrowID]->{'documentNxbName'}?></a></div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Năm phát hành - số <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><?=$documentList[$borrowID]->{'documentDatePH'}?> | <?=$documentList[$borrowID]->{'documentNumberPH'}?></div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Loại tài liệu <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><a href=""><?=$documentList[$borrowID]->{'documentCategoryName'}?></a></div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Ngôn ngữ <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><a href=""><?=$documentList[$borrowID]->{'documentLanguageName'}?></a></div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Giá bìa <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><?=$documentList[$borrowID]->{'documentPrice'}?> Vnd</div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Số lượng <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><?=$documentList[$borrowID]->{'documentQuantity'}?></div>
                        </div>
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-lg-6"><span class="badge badge-pill badge-info"> Vị trí <i class="ni ni-bold-right"></i><i class="ni ni-bold-right"></i></span></div>
                            <div class="col-lg-6"><a href=""><?=$documentList[$borrowID]->{'documentPlaceName'}?></a></div>
                        </div>
                        <hr class="my-3">
                        <p><?=$documentList[$borrowID]->{'documentContent'}?></p>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Thông tin người mượn</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="borrowBook.php" method="POST">
                            <div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Người mượn</label>
                                            <input type="text" id="userID" disabled style="color: blue" class="form-control form-control-alternative" value="<?=$_SESSION['username']?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Ngày mượn - ngày trả</label>
                                            <input type="text" required class="form-control" name="datetimes" />

                                            <script>
                                                $(function() {
                                                    $('input[name="datetimes"]').daterangepicker({
                                                        timePicker: true,
                                                        startDate: moment().startOf('hour'),
                                                        endDate: moment().startOf('hour').add(32, 'hour'),
                                                        locale: {
                                                            format: 'M/DD hh:mm A'
                                                        }
                                                    },function(start, end, label) {
                                                        document.getElementById("startDate").value = start.format('YYYY/MM/DD hh:mm:ss A');
                                                        document.getElementById("endDate").value = end.format('YYYY/MM/DD hh:mm:ss A');
                                                        checkDate();
                                                        console.log("A new date selection was made: " + start.format('YYYY/MM/DD hh:mm:ss A') + ' to ' + end.format('YYYY-MM-DD'));
                                                    });
                                                });
                                                function checkDate() {
                                                    var startDate = document.getElementById("startDate").value.split('T')[0];
                                                    startDate = startDate.replace(/-/g,'/');
                                                    var endDate = '2018/12/07';
                                                    endDate = endDate.replace(/-/g,'/');
                                                    console.log('-');
                                                    console.log(endDate);
                                                    var regExp = /(\d{1,2})\/(\d{1,2})\/(\d{2,4})/;
                                                    if (parseInt(endDate.replace(regExp, "$1$2$3")) <= parseInt(startDate.replace(regExp, "$1$2$3"))) {
                                                        document.getElementById("luu").disabled = false;
                                                    }else
                                                    {
                                                        alert("Invalid value for date");
                                                        document.getElementById("luu").disabled = true;
                                                    }
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="startDate" name="startDate">
                                <input type="hidden" id="endDate" name="endDate">
                                <input type="hidden" id="bookID" name="bookID" value="<?=$documentList[$borrowID]->{'documentID'};?>">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Số lượng</label>
                                            <input type="number" id="input-email" required min="1" max="<?=$documentList[$borrowID]->{'documentQuantity'}?>" name="soluong" class="form-control form-control-alternative" placeholder="1,2,3,..." value="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-first-name">Ghi chú</label>
                                            <input type="text" id="input-first-name" name="note" class="form-control form-control-alternative">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="margin: auto">
                                    <button type="submit" class="btn btn-danger" name="huy" value="cancel">Hủy</button>
                                    <button type="submit" class="btn btn-success" disabled name="luu" value="submit">Lưu</button>
                                </div>
                            </div>
                            <hr class="my-3">
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->

<!-- Modal -->
</body>

</html>

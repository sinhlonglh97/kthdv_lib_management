<?php
session_start();
if(isset($_SESSION['username'])){
    $role = $_SESSION['role'];
    if($role == 'student' || $role == 'collaborator'){
        header('Location: index.php');
    }
}
include_once("Controller/controller.php");
$controller = new Controller();
$authorList = $controller->getAuthors();

include_once("Controller/categoryController.php");
$controller = new categoryController();
$categoryList = $controller->category();

include_once("Controller/nxbController.php");
$controller = new nxbController();
$nxbList = $controller->nxb();

include_once("Controller/producerController.php");
$controller = new producerController();
$ProducerList = $controller->Producer();

include_once("Controller/placeController.php");
$controller = new placeController();
$placeList = $controller->place();

include_once("Controller/controller.php");
$controller = new Controller();
$languageList = $controller->getLanguages();

if(isset($_POST['huy'])){
    header('Location: document.php');
}else
    if(isset($_POST['luu'])){
        echo "<script>console.log($name)</script>";
        include_once("Controller/documentController.php");
        $controller = new documentController();
        $result = $controller->insert($_POST['documentName'],$_POST['documentQuantity'],$_POST['documentPrice'],
            $_POST['documentCountTB'],$_POST['documentSizePage'],$_POST['documentNumberPH'],$_POST['datePH'],
            $_POST['dateUpdate'],$_POST['documentAuthor'],$_POST['documentCategory'],$_POST['documentNxb'],
            $_POST['documentProducer'],$_POST['documentLanguage'],$_POST['documentPlace'],$_POST['documentContent']);
        header('Location: document.php');
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Argon Dashboard - Free Dashboard for Bootstrap 4</title>
    <!-- Favicon -->
    <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="./assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="./assets/css/argon.css?v=1.0.0" rel="stylesheet">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="./assets/js/argon.js?v=1.0.0"></script>
</head>

<body>
<!-- Sidenav -->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="../index.html">
            <img src="./assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="./assets/img/theme/team-1-800x800.jpg">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>My profile</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>Settings</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>Activity</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>Support</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="logout.php" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="../index.html">
                            <img src="./assets/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">
                        <i class="ni ni-tv-2 text-primary"></i> Trang chủ
                    </a>
                </li>
                <?php
                if($role!='student' && $role != 'collaborator'){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link " href="category.php">
                            <i class="ni ni-planet text-blue"></i> Loại tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producer.php">
                            <i class="ni ni-pin-3 text-orange"></i> Nhà sản xuất
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="nxb.php">
                            <i class="ni ni-single-02 text-yellow"></i> Nhà xuất bản
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listAuthor.php">
                            <i class="ni ni-single-02 text-yellow"></i> Danh sách tác giả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listLanguage.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Danh sách ngôn ngữ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listRoom.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Danh sách phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listRoomCategory.php">
                            <i class="ni ni-key-25 text-info"></i> Danh sách loại phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="place.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Vị trí tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listClass.php">
                            <i class="ni ni-planet text-blue"></i> Danh sách lớp
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listFaculty.php">
                            <i class="ni ni-planet text-blue"></i> Danh sách khoa
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="document.php">
                            <i class="ni ni-key-25 text-info"></i> Danh sách tài liệu
                        </a>
                    </li>
                    <?php
                }
                ?>

                <li class="nav-item">
                    <a class="nav-link" href="searchDocument.php">
                        <i class="ni ni-key-25 text-info"></i> Tra cứu - tìm tài liệu
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="searchRoom.php">
                        <i class="ni ni-key-25 text-info"></i> Đặt phòng
                    </a>
                </li>
                <?php
                if($role!='student' && $role != 'staff'){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="userDocument.php">
                            <i class="ni ni-circle-08 text-pink"></i> Danh sách mượn tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userRoom.php">
                            <i class="ni ni-circle-08 text-pink"></i> Danh sách đặt phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userDocumentWait.php">
                            <i class="ni ni-circle-08 text-pink"></i> Duyệt mượn tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userRoomWait.php">
                            <i class="ni ni-circle-08 text-pink"></i> Duyệt đặt phòng
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
        </div>
    </div>
</nav>
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../index.html">Thêm sách mới</a>
            <!-- Form -->
            <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input class="form-control" placeholder="Search" type="text">
                    </div>
                </div>
            </form>
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="./assets/img/theme/team-4-800x800.jpg">
                </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold"><?=$_SESSION['username']?></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-settings-gear-65"></i>
                            <span>Settings</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-calendar-grid-58"></i>
                            <span>Activity</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-support-16"></i>
                            <span>Support</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="logout.php" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">

    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class= "row">
                            <h3 class="mb-0">Thêm sách</h3>
                        </div>
                    </div>

                    <form style="margin: 5%" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-form-label">Tên sách</div>
                                <div class="form-group">
                                    <input type="text" name="documentName" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-form-label">Số lượng</div>
                                <div class="form-group">
                                    <input type="number" min="1" name="documentQuantity" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-form-label">Giá bìa</div>
                                <div class="form-group">
                                    <input type="number" name="documentPrice" min="1" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-form-label">Số lần tái bản</div>
                                <div class="form-group">
                                    <input type="number" name="documentCountTB" min="1" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-form-label">Kích thước khổ giấy</div>
                                <div class="form-group">
                                    <input type="text" name="documentSizePage" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-form-label">Số phát hành</div>
                                <div class="form-group">
                                    <input type="number" name="documentNumberPH" min="1" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-form-label">Ngày phát hành</div>
                                <div class="form-group">
                                    <input type="text" name="datePHview" class="form-control" value="10/24/1984" />
                                    <input type="hidden" id="datePH" name="datePH" class="form-control" />

                                    <script>
                                        $(function() {
                                            $('input[name="datePHview"]').daterangepicker({
                                                singleDatePicker: true,
                                                showDropdowns: true,
                                                minYear: 1901,
                                                maxYear: parseInt(moment().format('YYYY'),10)
                                            }, function(start, end, label) {
                                                var years = moment().diff(start, 'years');
                                                document.getElementById("datePH").value = start.format('YYYY/MM/DD');
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-form-label">Ngày cập nhật</div>
                                <div class="form-group">
                                    <input type="text" name="dateUpdateview" class="form-control" value="10/24/1984" />
                                    <input type="hidden" id="dateUpdate" name="dateUpdate" class="form-control" />

                                    <script>
                                        $(function() {
                                            $('input[name="dateUpdateview"]').daterangepicker({
                                                singleDatePicker: true,
                                                showDropdowns: true,
                                                minYear: 1901,
                                                maxYear: parseInt(moment().format('YYYY'),10)
                                            }, function(start, end, label) {
                                                var years = moment().diff(start, 'years');
                                                document.getElementById("dateUpdate").value = start.format('YYYY/MM/DD');
                                            });
                                        });
                                    </script>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-form-label">Tác giả</div>
                                <div class="form-group">
                                    <select type="text" name="documentAuthor" class="form-control">
                                        <?php
                                        foreach ($authorList as $item) {
                                            ?>
                                            <option name="" value="<?=$item->{'authorID'}?>"><?=$item->{'authorName'}?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-form-label">Loại tài liệu</div>
                                <div class="form-group">
                                    <select type="text" name="documentCategory" class="form-control">
                                        <?php
                                        foreach ($categoryList as $key => $item) {
                                            ?>
                                            <option value="<?=$item->{'categoryID'}?>" name=""><?=$item->{'categoryName'}?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-form-label">Nhà xuất bản</div>
                                <div class="form-group">
                                    <select type="text" name="documentNxb" class="form-control">
                                        <?php
                                        foreach ($nxbList as $item) {
                                            ?>
                                            <option name="" value="<?=$item->{'nxbID'}?>"><?=$item->{'nxbName'}?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-form-label">Nhà sản xuất</div>
                                <div class="form-group">
                                    <select type="text" name="documentProducer" class="form-control">
                                        <?php
                                        foreach ($ProducerList as $item) {
                                            ?>
                                            <option name=""value="<?=$item->{'producerID'}?>"><?=$item->{'producerName'}?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-form-label">Ngôn ngữ</div>
                                <div class="form-group">
                                    <select type="text" name="documentLanguage" class="form-control">
                                        <?php
                                        foreach ($languageList as $item) {
                                            ?>
                                            <option name=""value="<?=$item->{'languageID'}?>"><?=$item->{'name'}?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-form-label">Vị trí</div>
                                <div class="form-group">
                                    <select type="text" name="documentPlace" class="form-control">
                                        <?php
                                        foreach ($placeList as $item) {
                                            ?>
                                            <option name=""value="<?=$item->{'placeID'}?>"><?=$item->{'placeDes'}?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-form-label">Tóm tắt nội dung</div>
                                <div class="form-group">
                                    <textarea rows="5" name="documentContent" type="text" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div style="margin: auto">
                                <button type="submit" class="btn btn-danger" name="huy" value="cancel">Hủy</button>
                                <button type="submit" class="btn btn-success" name="luu" value="submit">Lưu</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- Dark table -->

        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">


            </div>
        </footer>
    </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<!-- Modal -->
</body>

</html>

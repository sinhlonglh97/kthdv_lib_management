-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2018 at 04:03 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `authorID` int(30) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL,
  `status` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`authorID`, `name`, `description`, `status`) VALUES
(1, 'Tran Van Tuan', 'Description made by tuan', '');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `categoryID` int(30) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL,
  `status` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `classID` int(30) NOT NULL,
  `facultyID` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `computer`
--

CREATE TABLE `computer` (
  `computerID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `computerCategoryID` int(11) NOT NULL,
  `desciption` text COLLATE utf32_bin NOT NULL,
  `status` varchar(255) COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `computercategory`
--

CREATE TABLE `computercategory` (
  `computerCategoryID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `documentID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `countTB` int(11) NOT NULL,
  `sizePaper` varchar(128) COLLATE utf32_bin NOT NULL,
  `content` text COLLATE utf32_bin NOT NULL,
  `price` int(11) NOT NULL,
  `numberPH` int(11) NOT NULL,
  `datePH` date NOT NULL,
  `quantity` int(11) NOT NULL,
  `dateUpdate` date NOT NULL,
  `categoryID` int(11) NOT NULL,
  `authorID` int(11) NOT NULL,
  `nxbID` int(11) NOT NULL,
  `producerID` int(11) NOT NULL,
  `placeID` int(11) NOT NULL,
  `languageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `facultyID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL,
  `status` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `languageID` int(30) NOT NULL,
  `name` varchar(50) COLLATE utf32_bin NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL,
  `status` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `nxb`
--

CREATE TABLE `nxb` (
  `nxbID` int(30) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE `place` (
  `placeID` int(11) NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL,
  `father` text COLLATE utf32_bin NOT NULL,
  `status` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `producer`
--

CREATE TABLE `producer` (
  `producerID` int(30) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL,
  `status` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `roomID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `roomCategoryID` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf32_bin NOT NULL,
  `desciption` varchar(255) COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `roomcategory`
--

CREATE TABLE `roomcategory` (
  `roomCategoryID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_bin NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(30) COLLATE utf32_bin NOT NULL,
  `password` varchar(30) COLLATE utf32_bin NOT NULL,
  `userID` int(11) NOT NULL,
  `email` varchar(128) COLLATE utf32_bin NOT NULL,
  `phone` varchar(128) COLLATE utf32_bin NOT NULL,
  `address` varchar(255) COLLATE utf32_bin NOT NULL,
  `birthday` date NOT NULL,
  `name` varchar(128) COLLATE utf32_bin NOT NULL,
  `function` varchar(128) COLLATE utf32_bin NOT NULL,
  `classID` int(11) NOT NULL,
  `facultyID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_computer`
--

CREATE TABLE `user_computer` (
  `userID` int(11) NOT NULL,
  `computerID` int(11) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `date` date NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_document`
--

CREATE TABLE `user_document` (
  `userID` int(11) NOT NULL,
  `documentID` int(11) NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_room`
--

CREATE TABLE `user_room` (
  `userID` int(11) NOT NULL,
  `roomID` int(11) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `date` date NOT NULL,
  `description` text COLLATE utf32_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`authorID`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryID`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`classID`),
  ADD KEY `facultyID` (`facultyID`);

--
-- Indexes for table `computer`
--
ALTER TABLE `computer`
  ADD PRIMARY KEY (`computerID`),
  ADD KEY `computerCategoryID` (`computerCategoryID`);

--
-- Indexes for table `computercategory`
--
ALTER TABLE `computercategory`
  ADD PRIMARY KEY (`computerCategoryID`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`documentID`),
  ADD KEY `categoryID` (`categoryID`),
  ADD KEY `authorID` (`authorID`),
  ADD KEY `nxbID` (`nxbID`),
  ADD KEY `placeID` (`placeID`),
  ADD KEY `producerID` (`producerID`),
  ADD KEY `languageID` (`languageID`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`facultyID`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`languageID`);

--
-- Indexes for table `nxb`
--
ALTER TABLE `nxb`
  ADD PRIMARY KEY (`nxbID`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`placeID`);

--
-- Indexes for table `producer`
--
ALTER TABLE `producer`
  ADD PRIMARY KEY (`producerID`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`roomID`),
  ADD KEY `roomCategoryID` (`roomCategoryID`);

--
-- Indexes for table `roomcategory`
--
ALTER TABLE `roomcategory`
  ADD PRIMARY KEY (`roomCategoryID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`),
  ADD KEY `classID` (`classID`),
  ADD KEY `facultyID` (`facultyID`);

--
-- Indexes for table `user_computer`
--
ALTER TABLE `user_computer`
  ADD PRIMARY KEY (`userID`,`computerID`),
  ADD KEY `computerID` (`computerID`);

--
-- Indexes for table `user_document`
--
ALTER TABLE `user_document`
  ADD PRIMARY KEY (`userID`,`documentID`),
  ADD KEY `documentID` (`documentID`);

--
-- Indexes for table `user_room`
--
ALTER TABLE `user_room`
  ADD PRIMARY KEY (`userID`,`roomID`),
  ADD KEY `roomID` (`roomID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_1` FOREIGN KEY (`facultyID`) REFERENCES `faculty` (`facultyID`);

--
-- Constraints for table `computer`
--
ALTER TABLE `computer`
  ADD CONSTRAINT `computer_ibfk_1` FOREIGN KEY (`computerCategoryID`) REFERENCES `computercategory` (`computerCategoryID`);

--
-- Constraints for table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `document_ibfk_1` FOREIGN KEY (`categoryID`) REFERENCES `category` (`categoryID`),
  ADD CONSTRAINT `document_ibfk_2` FOREIGN KEY (`authorID`) REFERENCES `author` (`authorID`),
  ADD CONSTRAINT `document_ibfk_3` FOREIGN KEY (`nxbID`) REFERENCES `nxb` (`nxbID`),
  ADD CONSTRAINT `document_ibfk_4` FOREIGN KEY (`placeID`) REFERENCES `place` (`placeID`),
  ADD CONSTRAINT `document_ibfk_5` FOREIGN KEY (`producerID`) REFERENCES `producer` (`producerID`),
  ADD CONSTRAINT `document_ibfk_6` FOREIGN KEY (`languageID`) REFERENCES `language` (`languageID`);

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`roomCategoryID`) REFERENCES `roomcategory` (`roomCategoryID`),
  ADD CONSTRAINT `room_ibfk_2` FOREIGN KEY (`roomCategoryID`) REFERENCES `roomcategory` (`roomCategoryID`),
  ADD CONSTRAINT `room_ibfk_3` FOREIGN KEY (`roomCategoryID`) REFERENCES `roomcategory` (`roomCategoryID`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`classID`) REFERENCES `class` (`classID`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`facultyID`) REFERENCES `faculty` (`facultyID`);

--
-- Constraints for table `user_computer`
--
ALTER TABLE `user_computer`
  ADD CONSTRAINT `user_computer_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `user_computer_ibfk_2` FOREIGN KEY (`computerID`) REFERENCES `computer` (`computerID`);

--
-- Constraints for table `user_document`
--
ALTER TABLE `user_document`
  ADD CONSTRAINT `user_document_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `user_document_ibfk_2` FOREIGN KEY (`documentID`) REFERENCES `document` (`documentID`),
  ADD CONSTRAINT `user_document_ibfk_3` FOREIGN KEY (`documentID`) REFERENCES `document` (`documentID`);

--
-- Constraints for table `user_room`
--
ALTER TABLE `user_room`
  ADD CONSTRAINT `user_room_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `user_room_ibfk_2` FOREIGN KEY (`roomID`) REFERENCES `room` (`roomID`),
  ADD CONSTRAINT `user_room_ibfk_3` FOREIGN KEY (`roomID`) REFERENCES `room` (`roomID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

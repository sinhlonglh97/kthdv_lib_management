<!DOCTYPE html>
<html>

<?php

session_start();
if(isset($_SESSION['username'])){
    $role = $_SESSION['role'];
    if($role == 'student' || $role == 'collaborator' || $role == 'staff'){
        header('Location: index.php');
    }
}
include_once("Controller/userController.php");

$controller = new userController();

$list = $controller->getUsers();
$length = count($list);
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>User</title>
    <!-- Favicon -->
    <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="./assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="./assets/css/argon.css?v=1.0.0" rel="stylesheet">
</head>

<body>
<!-- Sidenav -->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="./index.php">
            <img src="./assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="./assets/img/theme/team-1-800x800.jpg">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>My profile</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>Settings</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>Activity</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>Support</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="logout.php" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="../index.html">
                            <img src="./assets/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->

            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">
                        <i class="ni ni-tv-2 text-primary"></i> Trang chủ
                    </a>
                </li>
                <?php
                if($role!='student' && $role != 'collaborator'){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link " href="category.php">
                            <i class="ni ni-planet text-blue"></i> Loại tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producer.php">
                            <i class="ni ni-pin-3 text-orange"></i> Nhà sản xuất
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="nxb.php">
                            <i class="ni ni-single-02 text-yellow"></i> Nhà xuất bản
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listAuthor.php">
                            <i class="ni ni-single-02 text-yellow"></i> Danh sách tác giả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listLanguage.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Danh sách ngôn ngữ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listRoom.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Danh sách phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listRoomCategory.php">
                            <i class="ni ni-key-25 text-info"></i> Danh sách loại phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="place.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Vị trí tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listClass.php">
                            <i class="ni ni-planet text-blue"></i> Danh sách lớp
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listFaculty.php">
                            <i class="ni ni-planet text-blue"></i> Danh sách khoa
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="document.php">
                            <i class="ni ni-key-25 text-info"></i> Danh sách tài liệu
                        </a>
                    </li>
                    <?php
                }
                ?>

                <li class="nav-item">
                    <a class="nav-link" href="searchDocument.php">
                        <i class="ni ni-key-25 text-info"></i> Tra cứu - tìm tài liệu
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="searchRoom.php">
                        <i class="ni ni-key-25 text-info"></i> Đặt phòng
                    </a>
                </li>
                <?php
                if($role!='student' && $role != 'staff'){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="userDocument.php">
                            <i class="ni ni-circle-08 text-pink"></i> Danh sách mượn tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userRoom.php">
                            <i class="ni ni-circle-08 text-pink"></i> Danh sách đặt phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userDocumentWait.php">
                            <i class="ni ni-circle-08 text-pink"></i> Duyệt mượn tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userRoomWait.php">
                            <i class="ni ni-circle-08 text-pink"></i> Duyệt đặt phòng
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
        </div>
    </div>
</nav>
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->

    <!-- Header -->
    <div class="header bg-gradient-primary pb-6 pt-5 pt-md-8">
        <div class="container-fluid">

        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">List User</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="./addUser.php" class="btn btn-primary">Add User</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Password</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Birthday</th>
                                <th scope="col">Address</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            for($i = 0; $i < $length; $i++) {
                                ?>
                                <tr>
                                    <td>
                                        <?= $list[$i]->{'id'} ?>
                                    </td>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <span class="mb-0 text-sm"><?= $list[$i]->{'password'} ?></span>
                                        </div>

                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <span class="mb-0 text-sm"><?= $list[$i]->{'name'} ?></span>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <span class="mb-0 text-sm"><?= $list[$i]->{'email'} ?></span>
                                        </div>

                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <span class="mb-0 text-sm"><?= $list[$i]->{'phone'} ?></span>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <span class="mb-0 text-sm"><?= $list[$i]->{'birthday'} ?></span>
                                        </div>

                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <span class="mb-0 text-sm"><?= $list[$i]->{'address'} ?></span>
                                        </div>
                                    </th>

                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>


        <!-- Footer -->

    </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<script src="./assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="./assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Argon JS -->
<script src="./assets/js/argon.js?v=1.0.0"></script>
</body>

</html>

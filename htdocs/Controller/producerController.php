<?php
include_once("Dao/producerDao.php");
class producerController
{
    private $dao;
    public function __construct() {
        $this->dao = new producerDao();
    }

    public function Producer() {
        return $this->dao->getProducerList();
    }

    public  function update($id,$name,$status,$description){
        return $this->dao->updateProducer($id,$name,$status,$description);
    }

    public  function insert($id,$name,$status,$description){
        return $this->dao->insertProducer($id,$name,$status,$description);
    }

}
?>
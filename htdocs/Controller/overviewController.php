<?php
include_once("Dao/overviewDao.php");
class overviewController
{
    private $dao;
    public function __construct() {
        $this->dao = new overviewDao();
    }

    public function overview() {
        return $this->dao->getoverviewList();
    }

}
?>

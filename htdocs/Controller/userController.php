<?php
include_once("Dao/userDao.php");
class userController
{
    private $dao;
    public function __construct() {
        $this->dao = new userDao();
    }

    public function addUser($id,$name,$faculty ,$class, $username,$password,$email,$role) {

        $result = $this->dao->addUser($id,$name,$faculty ,$class, $username,$password,$email,$role);
        return $result;
    }
    public function editUser($id,$name,$email,$phone,$birthday, $address) {

        $result = $this->dao->editUser($id,$name,$email,$phone ,$birthday, $address);
        return $result;
    }

    public function getUsers() {
        $list = $this->dao->getUsers();
        return $list;
    }
}
?>

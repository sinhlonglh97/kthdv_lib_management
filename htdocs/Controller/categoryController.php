<?php include_once("Dao/categoryDao.php"); ?>
<?php
class categoryController {
    private $dao;
    public function __construct() {
        $this->dao = new categoryDao();
    }

    public function invoke() {
        $authorList = $this->dao->getAuthorList();
        include  'Views/dashboard/homepage.php';
    }

    public function category() {
       return $this->dao->getCategoryList();
    }

    public  function update($id,$name,$status,$description){
        return $this->dao->updateCategory($id,$name,$status,$description);
    }

    public  function insert($id,$name,$status,$description){
        return $this->dao->insertCategory($id,$name,$status,$description);
    }

}
?>

<?php include_once("Dao/dao.php");
class Controller {

    private $dao;

    public function __construct() {
        $this->dao = new Dao();
    }


    public function getAuthors() {
        $authorList = $this->dao->getAuthorList();
        return $authorList;
    }

    public function getAuthor($id)
    {
        $author = $this->dao->getAuthor($id);
        return $author;
    }

    public function addAuthor($name,$description) {

        $result = $this->dao->addAuthor($name,$description);
        return $result;
    }
    public function editAuthor($id,$name,$description)
    {
        $result = $this->dao->editAuthor($id,$name,$description);
        return $result;
    }

    public function login($username,$password)
    {
        $result = $this->dao->login($username,$password);
        return $result;
    }

    public function getLanguages() {
        $languageList = $this->dao->getLanguageList();
        return $languageList;
    }

    public function getLanguage($id)
    {
        $language = $this->dao->getLanguage($id);
        return $language;
    }

    public function addLanguage($name,$description)
    {
        $result = $this->dao->addLanguage($name,$description);
        return $result;
    }

    public function editLanguage($id,$name,$description)
    {
        $result = $this->dao->editLanguage($id,$name,$description);
        return $result;
    }



    public function getRoomCategories() {
        $list = $this->dao->getRoomCategoryList();
        return $list;
    }

    public function getRoomCategory($id)
    {
        $roomC = $this->dao->getRoomCategory($id);
        return $roomC;
    }

    public function editRoomCategory($id,$name,$description)
    {
        $result = $this->dao->editRoomCategory($id,$name,$description);
        return $result;
    }
    public function addRoomCategory($name,$description) {

        $result = $this->dao->addRoomCategory($name,$description);
        return $result;
    }




    public function addRoom($name,$description,$roomC , $status) {

        $result = $this->dao->addRoom($name,$description,$roomC , $status);
        return $result;
    }

    public function addFaculty($name,$description) {

        $result = $this->dao->addFaculty($name,$description);
        return $result;
    }

    public function getFaculties() {
        $list = $this->dao->getFacultyList();
        return $list;
    }

    public function getFaculty($id)
    {
        $r = $this->dao->getFaculty($id);
        return $r;
    }

    public function editFaculty($id,$name,$description)
    {
        $result = $this->dao->editFaculty($id,$name,$description);
        return $result;
    }

    public function addClass($name, $fID) {

        $result = $this->dao->addClass($name ,$fID);
        return $result;
    }

    public function getClass($id)
    {
        $r = $this->dao->getClass($id);
        return $r;
    }

    public function editClass($name,$fID)
    {
        $result = $this->dao->editClass($name,$fID);
        return $result;
    }

    public function getClasses() {
        $list = $this->dao->getClassList();
        return $list;
    }

    public function getRooms() {
        $list = $this->dao->getRoomList();
        return $list;
    }

    public function getRoom($id) {
        $list = $this->dao->getRoom($id);
        return $list;
    }

    public function editRoom($id,$name,$roomC ,$description, $status) {

        $result = $this->dao->editRoom($id,$name,$roomC ,$description, $status);
        return $result;
    }




}
?>

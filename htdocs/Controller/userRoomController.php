<?php
include_once("Dao/userRoomDao.php");
class userRoomController
{
    private $dao;
    public function __construct() {
        $this->dao = new userRoomDao();
    }

    public function userRoom() {
        return $this->dao->getuserRoomList();
    }

    public function userRoomWait() {
        return $this->dao->getuserRoomWaitList();
    }

    public  function update($id,$father,$status,$description){
        return $this->dao->updateuserRoom($id,$father,$status,$description);
    }

    public  function insert($id,$father,$status,$description){
        return $this->dao->insertuserRoom($id,$father,$status,$description);
    }
    public  function  returnRoom($userID,$RoomID){
        return $this->dao->returnRoom($userID,$RoomID);
    }
}
?>

<?php
include_once("Dao/roomDao.php");
class roomController
{
    private $dao;
    public function __construct() {
        $this->dao = new roomDao();
    }
    public function getRoomEmpty(){
        return $this->dao->getRoomEmpty();
    }
    public function search($value,$option){
        return $this->dao->search($value,$option);
    }
    public function borrow($roomID,$userID,$date,$timeStart,$timeEnd,$description){
        return $this->dao->borrow($roomID,$userID,$date,$timeStart,$timeEnd,$description);
    }
    public function accept($roomID,$userID,$date,$timeStart,$timeEnd,$description){
        return $this->dao->accept($roomID,$userID,$date,$timeStart,$timeEnd,$description);
    }
    public function deny($roomID,$userID,$date,$timeStart,$timeEnd,$description){
        return $this->dao->deny($roomID,$userID,$date,$timeStart,$timeEnd,$description);
    }
}
<?php
include_once("Dao/placeDao.php");
class placeController
{
    private $dao;
    public function __construct() {
        $this->dao = new placeDao();
    }

    public function place() {
        return $this->dao->getplaceList();
    }

    public  function update($id,$father,$status,$description){
        return $this->dao->updateplace($id,$father,$status,$description);
    }

    public  function insert($id,$father,$status,$description){
        return $this->dao->insertplace($id,$father,$status,$description);
    }

}
?>

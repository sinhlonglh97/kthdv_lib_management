<?php
include_once("Dao/nxbDao.php");
class nxbController
{
    private $dao;
    public function __construct() {
        $this->dao = new nxbDao();
    }

    public function nxb() {
        return $this->dao->getnxbList();
    }

    public  function update($id,$name,$status,$description){
        return $this->dao->updatenxb($id,$name,$status,$description);
    }

    public  function insert($id,$name,$status,$description){
        return $this->dao->insertnxb($id,$name,$status,$description);
    }

}
?>

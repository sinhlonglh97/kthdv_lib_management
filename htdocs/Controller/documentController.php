<?php
include_once("Dao/documentDao.php");
class documentController
{
    private $dao;
    public function __construct() {
        $this->dao = new documentDao();
    }

    public function document() {
        return $this->dao->getdocumentList();
    }

    public  function update($id,$father,$status,$description){
        return $this->dao->updatedocument($id,$father,$status,$description);
    }

    public  function insert($documentName,$documentQuantity,$documentPrice,
            $documentCountTB,$documentSizePage,$documentNumberPH,$datePH,
            $dateUpdate,$documentAuthor,$documentCategory,$documentNxb,
            $documentProducer,$documentLanguage,$documentPlace,$documentContent){
        return $this->dao->insertdocument($documentName,$documentQuantity,$documentPrice,
            $documentCountTB,$documentSizePage,$documentNumberPH,$datePH,
            $dateUpdate,$documentAuthor,$documentCategory,$documentNxb,
            $documentProducer,$documentLanguage,$documentPlace,$documentContent);
    }
    public function search($value,$option){
        return $this->dao->search($value,$option);
    }
    public function borrow($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate){
        return $this->dao->borrow($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate);
    }
    public function accept($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate){
        return $this->dao->accept($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate);
    }
    public function deny($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate){
        return $this->dao->deny($bookID,$userID,$startDate,$endDate,$description,$quantity,$quantityUpdate);
    }

}
?>

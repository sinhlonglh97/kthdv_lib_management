<?php
include_once("Dao/userDocumentDao.php");
class userDocumentController
{
    private $dao;
    public function __construct() {
        $this->dao = new userDocumentDao();
    }

    public function userDocument() {
        return $this->dao->getuserDocumentList();
    }

    public function userDocumentWait() {
        return $this->dao->getuserDocumentWaitList();
    }
    public  function update($id,$father,$status,$description){
        return $this->dao->updateuserDocument($id,$father,$status,$description);
    }

    public  function insert($id,$father,$status,$description){
        return $this->dao->insertuserDocument($id,$father,$status,$description);
    }
    public  function  returnDocument($userID,$documentID,$quantity){
        return $this->dao->returnDocument($userID,$documentID,$quantity);
    }

}
?>

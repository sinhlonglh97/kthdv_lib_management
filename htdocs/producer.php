<?php
session_start();
if(isset($_SESSION['username'])){
    $role = $_SESSION['role'];
    if($role == 'student' || $role == 'collaborator'){
        header('Location: index.php');
    }
}
include_once("Controller/producerController.php");
$controller = new producerController();


if(isset($_POST['name'])){
    $idnew = $_POST['id'];
    $namenew = $_POST['name'];
    echo "<script>console.log($namenew+' --')</script>";
    $controller->insert($_POST['id'],$_POST['name'],$_POST['status'],$_POST['description']);
}
$ProducerList = $controller->Producer();

?>
<script>

</script>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Argon Dashboard - Free Dashboard for Bootstrap 4</title>
    <!-- Favicon -->
    <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="./assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="./assets/css/argon.css?v=1.0.0" rel="stylesheet">
</head>

<body>
<!-- Sidenav -->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="../index.html">
            <img src="./assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="./assets/img/theme/team-1-800x800.jpg">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>My profile</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>Settings</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>Activity</span>
                    </a>
                    <a href="../examples/profile.html" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>Support</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="logout.php" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="../index.html">
                            <img src="./assets/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">
                        <i class="ni ni-tv-2 text-primary"></i> Trang chủ
                    </a>
                </li>
                <?php
                if($role!='student' && $role != 'collaborator'){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link " href="category.php">
                            <i class="ni ni-planet text-blue"></i> Loại tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producer.php">
                            <i class="ni ni-pin-3 text-orange"></i> Nhà sản xuất
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="nxb.php">
                            <i class="ni ni-single-02 text-yellow"></i> Nhà xuất bản
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listAuthor.php">
                            <i class="ni ni-single-02 text-yellow"></i> Danh sách tác giả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listLanguage.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Danh sách ngôn ngữ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listRoom.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Danh sách phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./listRoomCategory.php">
                            <i class="ni ni-key-25 text-info"></i> Danh sách loại phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="place.php">
                            <i class="ni ni-bullet-list-67 text-red"></i> Vị trí tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listClass.php">
                            <i class="ni ni-planet text-blue"></i> Danh sách lớp
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listFaculty.php">
                            <i class="ni ni-planet text-blue"></i> Danh sách khoa
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="document.php">
                            <i class="ni ni-key-25 text-info"></i> Danh sách tài liệu
                        </a>
                    </li>
                    <?php
                }
                ?>

                <li class="nav-item">
                    <a class="nav-link" href="searchDocument.php">
                        <i class="ni ni-key-25 text-info"></i> Tra cứu - tìm tài liệu
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="searchRoom.php">
                        <i class="ni ni-key-25 text-info"></i> Đặt phòng
                    </a>
                </li>
                <?php
                if($role!='student' && $role != 'staff'){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="userDocument.php">
                            <i class="ni ni-circle-08 text-pink"></i> Danh sách mượn tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userRoom.php">
                            <i class="ni ni-circle-08 text-pink"></i> Danh sách đặt phòng
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userDocumentWait.php">
                            <i class="ni ni-circle-08 text-pink"></i> Duyệt mượn tài liệu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="userRoomWait.php">
                            <i class="ni ni-circle-08 text-pink"></i> Duyệt đặt phòng
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
        </div>
    </div>
</nav>
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../index.html">Quản lý nhà sản xuất</a>
            <!-- Form -->
            <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input class="form-control" placeholder="Search" type="text">
                    </div>
                </div>
            </form>
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="./assets/img/theme/team-4-800x800.jpg">
                </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold"><?=$_SESSION['username']?></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-settings-gear-65"></i>
                            <span>Settings</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-calendar-grid-58"></i>
                            <span>Activity</span>
                        </a>
                        <a href="../examples/profile.html" class="dropdown-item">
                            <i class="ni ni-support-16"></i>
                            <span>Support</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="logout.php" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">

    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class= "row">
                            <h3 class="mb-0">Danh sách các nhà sản xuất</h3>
                            <button class="btn btn-icon btn-2 btn-info" style="margin-left:auto" type="button">
                                <span class="btn-inner--icon" data-toggle="modal" data-target="#insert">Thêm nhà sản xuất</span>
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive" >
                        <table class="table align-items-center table-flush" style=" text-align: center">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Mã nhà sản xuất</th>
                                <th scope="col">Tên nhà sản xuất</th>
                                <th scope="col">Mô tả</th>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Hành Động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($ProducerList as $item) {
                                ?>
                                <tr>
                                    <td>
                                        <?=$item->{'producerID'}?>
                                    </td>
                                    <td>
                                        <?=$item->{'producerName'}?>
                                    </td>
                                    <td>
                                        <?=$item->{'producerDes'}?>
                                    </td>
                                    <td>
                                  <span class="badge badge-dot mr-4">
                                    <i class="bg-warning"></i> <?=$item->{'producerStatus'}?>
                                  </span>
                                    </td>
                                    <td>
                                        <form action="updateProducer.php" method="POST">
                                            <input type="hidden" name="producerID" value="<?=$item->{'producerID'}?>">
                                            <input type="hidden" name="producerName" value="<?=$item->{'producerName'}?>">
                                            <input type="hidden" name="producerStatus" value="<?=$item->{'producerStatus'}?>">
                                            <input type="hidden" name="producerDes" value="<?=$item->{'producerDes'}?>">
                                            <button class="btn btn-icon btn-2 btn-primary" type="submit">
                                                Cập nhật
                                            </button>
                                        </form>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Dark table -->

        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">


            </div>
        </footer>
    </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<script src="./assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="./assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Argon JS -->
<script src="./assets/js/argon.js?v=1.0.0"></script>
<!-- Modal -->

<div class="modal fade" id="insert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm nhà sản xuất mới</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="Producer.php" style="margin: 5%" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-form-label">Mã nhà sản xuất</div>
                            <div class="form-group">
                                <?php $idNew = $ProducerList[count($ProducerList)-1]->{'producerID'}+1?>
                                <input type="text" disabled class="form-control" value="<?=$idNew?>" >
                                <input type="hidden" class="form-control" name="id" value="<?=$idNew?>" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-form-label">Tên nhà sản xuất</div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-form-label">Trạng thái</div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="status" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-form-label">Mô tả</div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="description">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Lưu</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

</body>

</html>
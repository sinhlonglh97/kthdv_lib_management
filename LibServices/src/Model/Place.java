package Model;

public class Place {
	private int placeID;
	private String placeFather;
	private String placeDes;
	private String placeStatus;
	public int getPlaceID() {
		return placeID;
	}
	public void setPlaceID(int placeID) {
		this.placeID = placeID;
	}
	public String getPlaceFather() {
		return placeFather;
	}
	public void setPlaceFather(String placeFather) {
		this.placeFather = placeFather;
	}
	public String getPlaceDes() {
		return placeDes;
	}
	public void setPlaceDes(String placeDes) {
		this.placeDes = placeDes;
	}
	public String getPlaceStatus() {
		return placeStatus;
	}
	public void setPlaceStatus(String placeStatus) {
		this.placeStatus = placeStatus;
	}
	
}

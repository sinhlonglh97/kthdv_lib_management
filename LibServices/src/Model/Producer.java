package Model;

public class Producer {

	private int producerID;
	private String producerName;
	private String producerDes;
	private String producerStatus;
	public int getProducerID() {
		return producerID;
	}
	public void setProducerID(int producerID) {
		this.producerID = producerID;
	}
	public String getProducerName() {
		return producerName;
	}
	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}
	public String getProducerDes() {
		return producerDes;
	}
	public void setProducerDes(String producerDes) {
		this.producerDes = producerDes;
	}
	public String getProducerStatus() {
		return producerStatus;
	}
	public void setProducerStatus(String producerStatus) {
		this.producerStatus = producerStatus;
	}
	
	
	
}

package Model;

public class OverView {
	private int countUser;
	private int countDoucument;
	private int countRoom;
	private int countComputer;
	private int countUserDocument;
	private int countUserComputer;
	private int countUserRoom;
	private int countComputerType;
	private int countCategory;
	private int countNxb;
	private int countPlacer;
	private int countAuthor;
	private int countPlace;
	private int countRoomTypr;
	private int countClass;
	private int countFaculty;
	public int getCountUser() {
		return countUser;
	}
	public void setCountUser(int countUser) {
		this.countUser = countUser;
	}
	public int getCountDoucument() {
		return countDoucument;
	}
	public void setCountDoucument(int countDoucument) {
		this.countDoucument = countDoucument;
	}
	public int getCountRoom() {
		return countRoom;
	}
	public void setCountRoom(int countRoom) {
		this.countRoom = countRoom;
	}
	public int getCountComputer() {
		return countComputer;
	}
	public void setCountComputer(int countComputer) {
		this.countComputer = countComputer;
	}
	public int getCountUserDocument() {
		return countUserDocument;
	}
	public void setCountUserDocument(int countUserDocument) {
		this.countUserDocument = countUserDocument;
	}
	public int getCountUserComputer() {
		return countUserComputer;
	}
	public void setCountUserComputer(int countUserComputer) {
		this.countUserComputer = countUserComputer;
	}
	public int getCountUserRoom() {
		return countUserRoom;
	}
	public void setCountUserRoom(int countUserRoom) {
		this.countUserRoom = countUserRoom;
	}
	public int getCountComputerType() {
		return countComputerType;
	}
	public void setCountComputerType(int countComputerType) {
		this.countComputerType = countComputerType;
	}
	public int getCountCategory() {
		return countCategory;
	}
	public void setCountCategory(int countCategory) {
		this.countCategory = countCategory;
	}
	public int getCountNxb() {
		return countNxb;
	}
	public void setCountNxb(int countNxb) {
		this.countNxb = countNxb;
	}
	public int getCountPlacer() {
		return countPlacer;
	}
	public void setCountPlacer(int countPlacer) {
		this.countPlacer = countPlacer;
	}
	public int getCountAuthor() {
		return countAuthor;
	}
	public void setCountAuthor(int countAuthor) {
		this.countAuthor = countAuthor;
	}
	public int getCountPlace() {
		return countPlace;
	}
	public void setCountPlace(int countPlace) {
		this.countPlace = countPlace;
	}
	public int getCountRoomTypr() {
		return countRoomTypr;
	}
	public void setCountRoomTypr(int countRoomTypr) {
		this.countRoomTypr = countRoomTypr;
	}
	public int getCountClass() {
		return countClass;
	}
	public void setCountClass(int countClass) {
		this.countClass = countClass;
	}
	public int getCountFaculty() {
		return countFaculty;
	}
	public void setCountFaculty(int countFaculty) {
		this.countFaculty = countFaculty;
	}
	
}

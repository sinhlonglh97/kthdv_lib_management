package Model;

public class Nxb {
	private int nxbID;
	private String nxbName;
	private String nxbDes;
	private String nxbStatus;
	
	public int getNxbID() {
		return nxbID;
	}
	public void setNxbID(int nxbID) {
		this.nxbID = nxbID;
	}
	public String getNxbName() {
		return nxbName;
	}
	public void setNxbName(String nxbName) {
		this.nxbName = nxbName;
	}
	public String getNxbDes() {
		return nxbDes;
	}
	public void setNxbDes(String nxbDes) {
		this.nxbDes = nxbDes;
	}
	public String getNxbStatus() {
		return nxbStatus;
	}
	public void setNxbStatus(String nxbStatus) {
		this.nxbStatus = nxbStatus;
	}
	
}

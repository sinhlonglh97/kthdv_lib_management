package Model;

public class Class {
	private int id;
	private int idFaculty;
	private String faculty;
	public String getFaculty() {
		return faculty;
	}
	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdFaculty() {
		return idFaculty;
	}
	public void setIdFaculty(int idFaculty) {
		this.idFaculty = idFaculty;
	}
}

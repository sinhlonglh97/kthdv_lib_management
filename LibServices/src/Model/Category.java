package Model;

public class Category {
	private int categoryID;
	private String categoryName;
	private String categoryDes;
	private String categoryStatus;
	
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryDes() {
		return categoryDes;
	}
	public void setCategoryDes(String categoryDes) {
		this.categoryDes = categoryDes;
	}
	public String getCategoryStatus() {
		return categoryStatus;
	}
	public void setCategoryStatus(String categoryStatus) {
		this.categoryStatus = categoryStatus;
	}
	
}

package Model;

public class Document {
	private int documentID;
	private String documentName;
	private int documentCountTB;
	private String documentSizePaper;
	private String documentContent;
	private int documentPrice;
	private int documentNumberPH;
	private String documentDatePH;
	private int documentQuantity;
	private String documentDateUpdate;
	private int documentCategoryID;
	
	private int documentAuthorID;
	private String documentAuthorName;
	private int documentNxbID;
	private String documentNxbName;
	private int documentProducerID;
	private int documentPlaceID;
	private int documentLanguageID;
	private String documentCategoryName;
	private String documentPlaceName;
	private String documentProducerName;
	private String documentLanguageName;
	
	
	public String getDocumentCategoryName() {
		return documentCategoryName;
	}
	public void setDocumentCategoryName(String documentCategoryName) {
		this.documentCategoryName = documentCategoryName;
	}
	public String getDocumentPlaceName() {
		return documentPlaceName;
	}
	public void setDocumentPlaceName(String documentPlaceName) {
		this.documentPlaceName = documentPlaceName;
	}
	public String getDocumentProducerName() {
		return documentProducerName;
	}
	public void setDocumentProducerName(String documentProducerName) {
		this.documentProducerName = documentProducerName;
	}
	public String getDocumentLanguageName() {
		return documentLanguageName;
	}
	public void setDocumentLanguageName(String documentLanguageName) {
		this.documentLanguageName = documentLanguageName;
	}
	public String getDocumentNxbName() {
		return documentNxbName;
	}
	public void setDocumentNxbName(String documentNxbName) {
		this.documentNxbName = documentNxbName;
	}
	public String getDocumentAuthorName() {
		return documentAuthorName;
	}
	public void setDocumentAuthorName(String documentAuthorName) {
		this.documentAuthorName = documentAuthorName;
	}
	public int getDocumentID() {
		return documentID;
	}
	public void setDocumentID(int documentID) {
		this.documentID = documentID;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public int getDocumentCountTB() {
		return documentCountTB;
	}
	public void setDocumentCountTB(int documentCountTB) {
		this.documentCountTB = documentCountTB;
	}
	public String getDocumentSizePaper() {
		return documentSizePaper;
	}
	public void setDocumentSizePaper(String documentSizePaper) {
		this.documentSizePaper = documentSizePaper;
	}
	public String getDocumentContent() {
		return documentContent;
	}
	public void setDocumentContent(String documentContent) {
		this.documentContent = documentContent;
	}
	public int getDocumentPrice() {
		return documentPrice;
	}
	public void setDocumentPrice(int documentPrice) {
		this.documentPrice = documentPrice;
	}
	public int getDocumentNumberPH() {
		return documentNumberPH;
	}
	public void setDocumentNumberPH(int documentNumberPH) {
		this.documentNumberPH = documentNumberPH;
	}
	public String getDocumentDatePH() {
		return documentDatePH;
	}
	public void setDocumentDatePH(String documentDatePH) {
		this.documentDatePH = documentDatePH;
	}
	public int getDocumentQuantity() {
		return documentQuantity;
	}
	public void setDocumentQuantity(int documentQuantity) {
		this.documentQuantity = documentQuantity;
	}
	public String getDocumentDateUpdate() {
		return documentDateUpdate;
	}
	public void setDocumentDateUpdate(String documentDateUpdate) {
		this.documentDateUpdate = documentDateUpdate;
	}
	public int getDocumentCategoryID() {
		return documentCategoryID;
	}
	public void setDocumentCategoryID(int documentCategoryID) {
		this.documentCategoryID = documentCategoryID;
	}
	public int getDocumentAuthorID() {
		return documentAuthorID;
	}
	public void setDocumentAuthorID(int documentAuthorID) {
		this.documentAuthorID = documentAuthorID;
	}
	public int getDocumentNxbID() {
		return documentNxbID;
	}
	public void setDocumentNxbID(int documentNxbID) {
		this.documentNxbID = documentNxbID;
	}
	public int getDocumentProducerID() {
		return documentProducerID;
	}
	public void setDocumentProducerID(int documentProducerID) {
		this.documentProducerID = documentProducerID;
	}
	public int getDocumentPlaceID() {
		return documentPlaceID;
	}
	public void setDocumentPlaceID(int documentPlaceID) {
		this.documentPlaceID = documentPlaceID;
	}
	public int getDocumentLanguageID() {
		return documentLanguageID;
	}
	public void setDocumentLanguageID(int documentLanguageID) {
		this.documentLanguageID = documentLanguageID;
	}
	
	
}

package MailServices;

import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {

	private void emailBorrowRoom(String email, String room, String start, String end)
	{
		final String username = "noreply.jsv@gmail.com";
		final String password = "6tfcvbnm";
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply.jsv@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject("Đặt phòng thành công");
			message.setText("Chào " + email + ","
					+ "\n\nBạn đã đặt thành công phòng "
					+ "\n\nThời gian sử dụng phòng từ "+ start +" đến "+ end+","
					+ "\n\nCảm ơn bạn đã sử dụng dịch vụ của thư viện chúng tôi"
					+ "\n\nTrân trọng,"
					+ "\nLibrarySystem");
			Transport.send(message);
			System.out.println("Done");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	private void emailBorrowDocument(String email, String document, int quantity, String start, String end)
	{
		final String username = "noreply.jsv@gmail.com";
		final String password = "6tfcvbnm";
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply.jsv@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject("Đặt phòng thành công");
			message.setText("Chào " + email + ","
					+ "\n\nBạn đã đặt thành công phòng "
					+ "\n\nThời gian mượn sách từ "+ start +" đến "+ end+","
					+ "\n\nSố lượng sách mượn là "+ quantity+" cuốn,"
					+ "\n\nCảm ơn bạn đã sử dụng dịch vụ của thư viện chúng tôi"
					+ "\n\nTrân trọng,"
					+ "\nLibrarySystem");
			Transport.send(message);
			System.out.println("Done");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
}

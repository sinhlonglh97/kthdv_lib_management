package LibServiceStudent;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import Model.Document;
import Model.UserDocument;
import MailServices.Mail;

@Path("/DocumentServices")
public class DocumentServices {
	private static Connection conn = DBConnection.Connect.getConneted(); 
	
	
	@POST
	@Path("/document/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Document> getDocument() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"SELECT document.*,"
					+ "author.name as authorName,"
					+ "nxb.name as nxbName,"
					+ "place.description as placeName,"
					+ "producer.name as producerName,"
					+ "category.name as categoryName,"
					+ "language.name as languageName"
					+ " FROM document"
					+ " inner join author on document.authorID = author.authorID"
					+ " inner join nxb on document.nxbID = nxb.nxbID"
					+ " inner join place on document.placeID = place.placeID"
					+ " inner join producer on document.producerID = producer.producerID"
					+ " inner join category on document.categoryID = category.categoryID"
					+ " inner join language on document.languageID = language.languageID");
			while (rs.next())
			{
				Document Document = new Document();
				Document.setDocumentID(rs.getInt("documentID"));
				Document.setDocumentName(rs.getString("name"));
				Document.setDocumentCountTB(rs.getInt("countTB"));
				Document.setDocumentSizePaper(rs.getString("sizePaper"));
				Document.setDocumentContent(rs.getString("content"));
				Document.setDocumentPrice(rs.getInt("price"));
				Document.setDocumentNumberPH(rs.getInt("numberPH"));
				Document.setDocumentDatePH(rs.getString("datePH"));
				Document.setDocumentQuantity(rs.getInt("quantity"));
				Document.setDocumentDateUpdate(rs.getString("dateUpdate"));
				Document.setDocumentCategoryID(rs.getInt("categoryID"));
				Document.setDocumentAuthorID(rs.getInt("authorID"));
				Document.setDocumentAuthorName(rs.getString("authorName"));
				Document.setDocumentNxbName(rs.getString("nxbName"));
				Document.setDocumentNxbID(rs.getInt("nxbID"));
				Document.setDocumentProducerID(rs.getInt("producerID"));
				Document.setDocumentPlaceID(rs.getInt("placeID"));
				Document.setDocumentLanguageID(rs.getInt("languageID"));
				Document.setDocumentProducerName(rs.getString("producerName"));
				Document.setDocumentCategoryName(rs.getString("categoryName"));
				Document.setDocumentLanguageName(rs.getString("languageName"));
				Document.setDocumentPlaceName(rs.getString("placeName"));
				DocumentList.add(Document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return DocumentList;
	}
	
	@POST
	@Path("/search/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Document> searchDocument(@FormParam("value") String value,@FormParam("option") String option) {
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		if(option.equals("name")) {
			DocumentList = searchByName(value);
		}else
			if(option.equals("author")) {
				DocumentList = searchByAuthor(value);
			}else
				if(option.equals("nxb")) {
					DocumentList = searchByNxb(value);
				}else
					if(option.equals("producer")) {
						DocumentList = searchByProducer(value);
					}else
						if(option.equals("place")) {
							DocumentList = searchByPlace(value);
						}else
							if(option.equals("language")) {
								DocumentList = searchByLanguage(value);
							}else
								if(option.equals("category")) {
									DocumentList = searchByCategory(value);
								}
		return DocumentList;
	}
	
	public static ArrayList<Document> searchByName(String value){
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"SELECT document.*,"
					+ "author.name as authorName,"
					+ "nxb.name as nxbName,"
					+ "place.description as placeName,"
					+ "producer.name as producerName,"
					+ "category.name as categoryName,"
					+ "language.name as languageName"
					+ " FROM document"
					+ " inner join author on document.authorID = author.authorID"
					+ " inner join nxb on document.nxbID = nxb.nxbID"
					+ " inner join place on document.placeID = place.placeID"
					+ " inner join producer on document.producerID = producer.producerID"
					+ " inner join category on document.categoryID = category.categoryID"
					+ " inner join language on document.languageID = language.languageID"
					+ " WHERE document.name LIKE '%" + value + "%'");
			while (rs.next())
			{
				Document Document = new Document();
				Document.setDocumentID(rs.getInt("documentID"));
				Document.setDocumentName(rs.getString("name"));
				Document.setDocumentCountTB(rs.getInt("countTB"));
				Document.setDocumentSizePaper(rs.getString("sizePaper"));
				Document.setDocumentContent(rs.getString("content"));
				Document.setDocumentPrice(rs.getInt("price"));
				Document.setDocumentNumberPH(rs.getInt("numberPH"));
				Document.setDocumentDatePH(rs.getString("datePH"));
				Document.setDocumentQuantity(rs.getInt("quantity"));
				Document.setDocumentDateUpdate(rs.getString("dateUpdate"));
				Document.setDocumentCategoryID(rs.getInt("categoryID"));
				Document.setDocumentAuthorID(rs.getInt("authorID"));
				Document.setDocumentAuthorName(rs.getString("authorName"));
				Document.setDocumentNxbName(rs.getString("nxbName"));
				Document.setDocumentNxbID(rs.getInt("nxbID"));
				Document.setDocumentProducerID(rs.getInt("producerID"));
				Document.setDocumentPlaceID(rs.getInt("placeID"));
				Document.setDocumentLanguageID(rs.getInt("languageID"));
				Document.setDocumentProducerName(rs.getString("producerName"));
				Document.setDocumentCategoryName(rs.getString("categoryName"));
				Document.setDocumentLanguageName(rs.getString("languageName"));
				Document.setDocumentPlaceName(rs.getString("placeName"));
				DocumentList.add(Document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return DocumentList;
	}
	public static ArrayList<Document> searchByAuthor(String value){
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"SELECT document.*,"
					+ "author.name as authorName,"
					+ "nxb.name as nxbName,"
					+ "place.description as placeName,"
					+ "producer.name as producerName,"
					+ "category.name as categoryName,"
					+ "language.name as languageName"
					+ " FROM document"
					+ " inner join author on document.authorID = author.authorID"
					+ " inner join nxb on document.nxbID = nxb.nxbID"
					+ " inner join place on document.placeID = place.placeID"
					+ " inner join producer on document.producerID = producer.producerID"
					+ " inner join category on document.categoryID = category.categoryID"
					+ " inner join language on document.languageID = language.languageID"
					+ " WHERE author.name LIKE '%" + value + "%'");
			while (rs.next())
			{
				Document Document = new Document();
				Document.setDocumentID(rs.getInt("documentID"));
				Document.setDocumentName(rs.getString("name"));
				Document.setDocumentCountTB(rs.getInt("countTB"));
				Document.setDocumentSizePaper(rs.getString("sizePaper"));
				Document.setDocumentContent(rs.getString("content"));
				Document.setDocumentPrice(rs.getInt("price"));
				Document.setDocumentNumberPH(rs.getInt("numberPH"));
				Document.setDocumentDatePH(rs.getString("datePH"));
				Document.setDocumentQuantity(rs.getInt("quantity"));
				Document.setDocumentDateUpdate(rs.getString("dateUpdate"));
				Document.setDocumentCategoryID(rs.getInt("categoryID"));
				Document.setDocumentAuthorID(rs.getInt("authorID"));
				Document.setDocumentAuthorName(rs.getString("authorName"));
				Document.setDocumentNxbName(rs.getString("nxbName"));
				Document.setDocumentNxbID(rs.getInt("nxbID"));
				Document.setDocumentProducerID(rs.getInt("producerID"));
				Document.setDocumentPlaceID(rs.getInt("placeID"));
				Document.setDocumentLanguageID(rs.getInt("languageID"));
				Document.setDocumentProducerName(rs.getString("producerName"));
				Document.setDocumentCategoryName(rs.getString("categoryName"));
				Document.setDocumentLanguageName(rs.getString("languageName"));
				Document.setDocumentPlaceName(rs.getString("placeName"));
				DocumentList.add(Document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return DocumentList;
	}
	public static ArrayList<Document> searchByNxb(String value){
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"SELECT document.*,"
					+ "author.name as authorName,"
					+ "nxb.name as nxbName,"
					+ "place.description as placeName,"
					+ "producer.name as producerName,"
					+ "category.name as categoryName,"
					+ "language.name as languageName"
					+ " FROM document"
					+ " inner join author on document.authorID = author.authorID"
					+ " inner join nxb on document.nxbID = nxb.nxbID"
					+ " inner join place on document.placeID = place.placeID"
					+ " inner join producer on document.producerID = producer.producerID"
					+ " inner join category on document.categoryID = category.categoryID"
					+ " inner join language on document.languageID = language.languageID"
					+ " WHERE nxb.name LIKE '%" + value + "%'");
			while (rs.next())
			{
				Document Document = new Document();
				Document.setDocumentID(rs.getInt("documentID"));
				Document.setDocumentName(rs.getString("name"));
				Document.setDocumentCountTB(rs.getInt("countTB"));
				Document.setDocumentSizePaper(rs.getString("sizePaper"));
				Document.setDocumentContent(rs.getString("content"));
				Document.setDocumentPrice(rs.getInt("price"));
				Document.setDocumentNumberPH(rs.getInt("numberPH"));
				Document.setDocumentDatePH(rs.getString("datePH"));
				Document.setDocumentQuantity(rs.getInt("quantity"));
				Document.setDocumentDateUpdate(rs.getString("dateUpdate"));
				Document.setDocumentCategoryID(rs.getInt("categoryID"));
				Document.setDocumentAuthorID(rs.getInt("authorID"));
				Document.setDocumentAuthorName(rs.getString("authorName"));
				Document.setDocumentNxbName(rs.getString("nxbName"));
				Document.setDocumentNxbID(rs.getInt("nxbID"));
				Document.setDocumentProducerID(rs.getInt("producerID"));
				Document.setDocumentPlaceID(rs.getInt("placeID"));
				Document.setDocumentLanguageID(rs.getInt("languageID"));
				Document.setDocumentProducerName(rs.getString("producerName"));
				Document.setDocumentCategoryName(rs.getString("categoryName"));
				Document.setDocumentLanguageName(rs.getString("languageName"));
				Document.setDocumentPlaceName(rs.getString("placeName"));
				DocumentList.add(Document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return DocumentList;
	}
	public static ArrayList<Document> searchByProducer(String value){
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"SELECT document.*,"
					+ "author.name as authorName,"
					+ "nxb.name as nxbName,"
					+ "place.description as placeName,"
					+ "producer.name as producerName,"
					+ "category.name as categoryName,"
					+ "language.name as languageName"
					+ " FROM document"
					+ " inner join author on document.authorID = author.authorID"
					+ " inner join nxb on document.nxbID = nxb.nxbID"
					+ " inner join place on document.placeID = place.placeID"
					+ " inner join producer on document.producerID = producer.producerID"
					+ " inner join category on document.categoryID = category.categoryID"
					+ " inner join language on document.languageID = language.languageID"
					+ " WHERE producer.name LIKE '%" + value + "%'");
			while (rs.next())
			{
				Document Document = new Document();
				Document.setDocumentID(rs.getInt("documentID"));
				Document.setDocumentName(rs.getString("name"));
				Document.setDocumentCountTB(rs.getInt("countTB"));
				Document.setDocumentSizePaper(rs.getString("sizePaper"));
				Document.setDocumentContent(rs.getString("content"));
				Document.setDocumentPrice(rs.getInt("price"));
				Document.setDocumentNumberPH(rs.getInt("numberPH"));
				Document.setDocumentDatePH(rs.getString("datePH"));
				Document.setDocumentQuantity(rs.getInt("quantity"));
				Document.setDocumentDateUpdate(rs.getString("dateUpdate"));
				Document.setDocumentCategoryID(rs.getInt("categoryID"));
				Document.setDocumentAuthorID(rs.getInt("authorID"));
				Document.setDocumentAuthorName(rs.getString("authorName"));
				Document.setDocumentNxbName(rs.getString("nxbName"));
				Document.setDocumentNxbID(rs.getInt("nxbID"));
				Document.setDocumentProducerID(rs.getInt("producerID"));
				Document.setDocumentPlaceID(rs.getInt("placeID"));
				Document.setDocumentLanguageID(rs.getInt("languageID"));
				Document.setDocumentProducerName(rs.getString("producerName"));
				Document.setDocumentCategoryName(rs.getString("categoryName"));
				Document.setDocumentLanguageName(rs.getString("languageName"));
				Document.setDocumentPlaceName(rs.getString("placeName"));
				DocumentList.add(Document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return DocumentList;
	}
	public static ArrayList<Document> searchByPlace(String value){
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"SELECT document.*,"
					+ "author.name as authorName,"
					+ "nxb.name as nxbName,"
					+ "place.description as placeName,"
					+ "producer.name as producerName,"
					+ "category.name as categoryName,"
					+ "language.name as languageName"
					+ " FROM document"
					+ " inner join author on document.authorID = author.authorID"
					+ " inner join nxb on document.nxbID = nxb.nxbID"
					+ " inner join place on document.placeID = place.placeID"
					+ " inner join producer on document.producerID = producer.producerID"
					+ " inner join category on document.categoryID = category.categoryID"
					+ " inner join language on document.languageID = language.languageID"
					+ " WHERE place.description LIKE '%" + value + "%'");
			while (rs.next())
			{
				Document Document = new Document();
				Document.setDocumentID(rs.getInt("documentID"));
				Document.setDocumentName(rs.getString("name"));
				Document.setDocumentCountTB(rs.getInt("countTB"));
				Document.setDocumentSizePaper(rs.getString("sizePaper"));
				Document.setDocumentContent(rs.getString("content"));
				Document.setDocumentPrice(rs.getInt("price"));
				Document.setDocumentNumberPH(rs.getInt("numberPH"));
				Document.setDocumentDatePH(rs.getString("datePH"));
				Document.setDocumentQuantity(rs.getInt("quantity"));
				Document.setDocumentDateUpdate(rs.getString("dateUpdate"));
				Document.setDocumentCategoryID(rs.getInt("categoryID"));
				Document.setDocumentAuthorID(rs.getInt("authorID"));
				Document.setDocumentAuthorName(rs.getString("authorName"));
				Document.setDocumentNxbName(rs.getString("nxbName"));
				Document.setDocumentNxbID(rs.getInt("nxbID"));
				Document.setDocumentProducerID(rs.getInt("producerID"));
				Document.setDocumentPlaceID(rs.getInt("placeID"));
				Document.setDocumentLanguageID(rs.getInt("languageID"));
				Document.setDocumentProducerName(rs.getString("producerName"));
				Document.setDocumentCategoryName(rs.getString("categoryName"));
				Document.setDocumentLanguageName(rs.getString("languageName"));
				Document.setDocumentPlaceName(rs.getString("placeName"));
				DocumentList.add(Document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return DocumentList;
	}
	public static ArrayList<Document> searchByLanguage(String value){
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"SELECT document.*,"
					+ "author.name as authorName,"
					+ "nxb.name as nxbName,"
					+ "place.description as placeName,"
					+ "producer.name as producerName,"
					+ "category.name as categoryName,"
					+ "language.name as languageName"
					+ " FROM document"
					+ " inner join author on document.authorID = author.authorID"
					+ " inner join nxb on document.nxbID = nxb.nxbID"
					+ " inner join place on document.placeID = place.placeID"
					+ " inner join producer on document.producerID = producer.producerID"
					+ " inner join category on document.categoryID = category.categoryID"
					+ " inner join language on document.languageID = language.languageID"
					+ " WHERE language.name LIKE '%" + value + "%'");
			while (rs.next())
			{
				Document Document = new Document();
				Document.setDocumentID(rs.getInt("documentID"));
				Document.setDocumentName(rs.getString("name"));
				Document.setDocumentCountTB(rs.getInt("countTB"));
				Document.setDocumentSizePaper(rs.getString("sizePaper"));
				Document.setDocumentContent(rs.getString("content"));
				Document.setDocumentPrice(rs.getInt("price"));
				Document.setDocumentNumberPH(rs.getInt("numberPH"));
				Document.setDocumentDatePH(rs.getString("datePH"));
				Document.setDocumentQuantity(rs.getInt("quantity"));
				Document.setDocumentDateUpdate(rs.getString("dateUpdate"));
				Document.setDocumentCategoryID(rs.getInt("categoryID"));
				Document.setDocumentAuthorID(rs.getInt("authorID"));
				Document.setDocumentAuthorName(rs.getString("authorName"));
				Document.setDocumentNxbName(rs.getString("nxbName"));
				Document.setDocumentNxbID(rs.getInt("nxbID"));
				Document.setDocumentProducerID(rs.getInt("producerID"));
				Document.setDocumentPlaceID(rs.getInt("placeID"));
				Document.setDocumentLanguageID(rs.getInt("languageID"));
				Document.setDocumentProducerName(rs.getString("producerName"));
				Document.setDocumentCategoryName(rs.getString("categoryName"));
				Document.setDocumentLanguageName(rs.getString("languageName"));
				Document.setDocumentPlaceName(rs.getString("placeName"));
				DocumentList.add(Document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return DocumentList;
	}
	public static ArrayList<Document> searchByCategory(String value){
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Document> DocumentList = new ArrayList<Document>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(
					"SELECT document.*,"
					+ "author.name as authorName,"
					+ "nxb.name as nxbName,"
					+ "place.description as placeName,"
					+ "producer.name as producerName,"
					+ "category.name as categoryName,"
					+ "language.name as languageName"
					+ " FROM document"
					+ " inner join author on document.authorID = author.authorID"
					+ " inner join nxb on document.nxbID = nxb.nxbID"
					+ " inner join place on document.placeID = place.placeID"
					+ " inner join producer on document.producerID = producer.producerID"
					+ " inner join category on document.categoryID = category.categoryID"
					+ " inner join language on document.languageID = language.languageID"
					+ " WHERE category.name LIKE '%" + value + "%'");
			while (rs.next())
			{
				Document Document = new Document();
				Document.setDocumentID(rs.getInt("documentID"));
				Document.setDocumentName(rs.getString("name"));
				Document.setDocumentCountTB(rs.getInt("countTB"));
				Document.setDocumentSizePaper(rs.getString("sizePaper"));
				Document.setDocumentContent(rs.getString("content"));
				Document.setDocumentPrice(rs.getInt("price"));
				Document.setDocumentNumberPH(rs.getInt("numberPH"));
				Document.setDocumentDatePH(rs.getString("datePH"));
				Document.setDocumentQuantity(rs.getInt("quantity"));
				Document.setDocumentDateUpdate(rs.getString("dateUpdate"));
				Document.setDocumentCategoryID(rs.getInt("categoryID"));
				Document.setDocumentAuthorID(rs.getInt("authorID"));
				Document.setDocumentAuthorName(rs.getString("authorName"));
				Document.setDocumentNxbName(rs.getString("nxbName"));
				Document.setDocumentNxbID(rs.getInt("nxbID"));
				Document.setDocumentProducerID(rs.getInt("producerID"));
				Document.setDocumentPlaceID(rs.getInt("placeID"));
				Document.setDocumentLanguageID(rs.getInt("languageID"));
				Document.setDocumentProducerName(rs.getString("producerName"));
				Document.setDocumentCategoryName(rs.getString("categoryName"));
				Document.setDocumentLanguageName(rs.getString("languageName"));
				Document.setDocumentPlaceName(rs.getString("placeName"));
				DocumentList.add(Document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return DocumentList;
	}
	@POST
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean update(@FormParam("id") int id, @FormParam("father") String father,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "update Document set DocumentID = ?,father = ?,status = ?, description = ? where DocumentID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, father);
		      preparedStmt.setString(3, status);
		      preparedStmt.setString(4, description);
		      preparedStmt.setInt(5, id);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
	
	@POST
	@Path("/insert/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean insert(
			@FormParam("documentName") String documentName,@FormParam("documentQuantity") int documentQuantity,
			@FormParam("documentPrice") int documentPrice,
            @FormParam("documentCountTB") int documentCountTB,@FormParam("documentSizePage") String documentSizePage,
            @FormParam("documentNumberPH") int documentNumberPH,@FormParam("datePH") String datePH,
            @FormParam("dateUpdate") String dateUpdate,@FormParam("documentAuthor") int documentAuthor,
            @FormParam("documentCategory") int documentCategory,@FormParam("documentNxb") int documentNxb,
            @FormParam("documentProducer") int documentProducer,@FormParam("documentLanguage") int documentLanguage,
            @FormParam("documentPlace") int documentPlace, @FormParam("documentContent") String documentContent
			)
	{
		System.out.println("in");
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select COUNT(*) from document");
			rs.next();
			int id = rs.getInt(1);
			String query = "INSERT INTO document (documentID, name, countTB, sizePaper, content, price, numberPH, datePH, quantity, dateUpdate, categoryID, authorID, nxbID, producerID, placeID, languageID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1,id);
		      preparedStmt.setString(2, documentName);
		      preparedStmt.setString(5, documentContent);
		      preparedStmt.setInt(9,documentQuantity);
		      preparedStmt.setInt(6, documentPrice);
		      preparedStmt.setInt(3,documentCountTB);
		      preparedStmt.setString(4, documentSizePage);
		      preparedStmt.setInt(7, documentNumberPH);
		      preparedStmt.setString(8, datePH);
		      preparedStmt.setString(10, dateUpdate);
		      preparedStmt.setInt(11, documentCategory);
		      preparedStmt.setInt(12, documentAuthor);
		      preparedStmt.setInt(13, documentNxb);
		      preparedStmt.setInt(14, documentProducer);
		      preparedStmt.setInt(15, documentPlace);
		      preparedStmt.setInt(16, documentLanguage);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
	@POST
	@Path("/borrow/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean borrow(@FormParam("bookID") int bookID, @FormParam("userID") int userID,
			@FormParam("startDate") String startDate,@FormParam("endDate") String endDate,
			@FormParam("description") String description,@FormParam("quantity") int quantity,
			@FormParam("quantityUpdate") int quantityUpdate)
	{
		System.out.println("in");
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "insert into user_document_wait(documentID,userID,startDate,endDate,description,quantity) values (?,?,?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, bookID);
		      preparedStmt.setInt(2, userID);
		      preparedStmt.setString(3, startDate);
		      preparedStmt.setString(4, endDate);
		      preparedStmt.setString(5, description);
		      preparedStmt.setInt(6, quantity);
		      preparedStmt.executeUpdate();
//		    String query2 = "update document set quantity = quantity- ? where documentID = ?";
//		      PreparedStatement preparedStmt2 = conn.prepareStatement(query2);
//		      preparedStmt2.setInt(1,quantity);
//		      preparedStmt2.setInt(2, bookID);
//		      preparedStmt2.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}

	@Path("/accept/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserDocument> accept(@FormParam("bookID") int bookID, @FormParam("userID") int userID,
			@FormParam("startDate") String startDate,@FormParam("endDate") String endDate,
			@FormParam("description") String description,@FormParam("quantity") int quantity,
			@FormParam("quantityUpdate") int quantityUpdate)
	{
		System.out.println("in");
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		String email = null;
		String document = null;
		try {
			String query = "insert into user_document(documentID,userID,startDate,endDate,description,quantity) values (?,?,?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, bookID);
		      preparedStmt.setInt(2, userID);
		      preparedStmt.setString(3, startDate);
		      preparedStmt.setString(4, endDate);
		      preparedStmt.setString(5, description);
		      preparedStmt.setInt(6, quantity);
		      preparedStmt.executeUpdate();
		    String query2 = "update document set quantity = quantity- ? where documentID = ?";
		      PreparedStatement preparedStmt2 = conn.prepareStatement(query2);
		      preparedStmt2.setInt(1,quantity);
		      preparedStmt2.setInt(2, bookID);
		      preparedStmt2.executeUpdate();
		    String query3 = "delete from user_document_wait where userID = ? and documentID = ?";
		      PreparedStatement preparedStmt3 = conn.prepareStatement(query3);
		      preparedStmt3.setInt(1, userID);
		      preparedStmt3.setInt(2, bookID);
		      preparedStmt3.executeUpdate();
		      stmt = conn.createStatement();
				rs = stmt.executeQuery(
						"SELECT name from document where documentID = "+ bookID);
				while (rs.next())
				{
					document = rs.getString("name");
				}
				rs2 = stmt.executeQuery(
						"SELECT email from user where userID = "+ userID);
				while (rs2.next())
				{
					email = rs2.getString("email");
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (rs2 != null) rs2.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		emailBorrowDocument(email, document, quantity, startDate, endDate);
		return getUserDocumentWait();
	}
	private void emailBorrowDocument(String email, String document, int quantity, String start, String end)
	{
		final String username = "noreply.jsv@gmail.com";
		final String password = "6tfcvbnm";
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply.jsv@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject("Borrow book is successful");
			message.setText("Dear " + email + ","
					+ "\n\nYou have successfully borrow " + document
					+ "\n\nTime borrowing from "+ start +" to "+ end+","
					+ "\n\nThe quantity is "+ quantity+" books,"
					+ "\n\nThank you for using the library's services"
					+ "\n\nRegards,"
					+ "\nLibrarySystem");
			Transport.send(message);
			System.out.println("Done");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	@Path("/deny/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserDocument> deny(@FormParam("bookID") int bookID, @FormParam("userID") int userID,
			@FormParam("startDate") String startDate,@FormParam("endDate") String endDate,
			@FormParam("description") String description,@FormParam("quantity") int quantity,
			@FormParam("quantityUpdate") int quantityUpdate)
	{
		System.out.println("in");
		Statement stmt = null;
		ResultSet rs = null;
		try {
		    String query3 = "delete from user_document_wait where userID = ? and documentID = ?";
		      PreparedStatement preparedStmt3 = conn.prepareStatement(query3);
		      preparedStmt3.setInt(1, userID);
		      preparedStmt3.setInt(2, bookID);
		      preparedStmt3.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return getUserDocumentWait();
	}
	public ArrayList<UserDocument> getUserDocumentWait() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<UserDocument> UserDocumentList = new ArrayList<UserDocument>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT user_document_wait.*,user.name as userName,document.name as documentName"
					+ " FROM user_document_wait"
					+ " inner join user on user_document_wait.userID = user.userID"
					+ " inner join document on user_document_wait.documentID = document.documentID");
			while (rs.next())
			{
				UserDocument UserDocument = new UserDocument();
				UserDocument.setUserID(rs.getInt("userID"));
				UserDocument.setDocumentID(rs.getInt("documentID"));
				UserDocument.setUserName(rs.getString("userName"));
				UserDocument.setDocumentName(rs.getString("documentName"));
				UserDocument.setStartDate(rs.getString("startDate"));
				UserDocument.setEndDate(rs.getString("endDate"));
				UserDocument.setQuantity(rs.getInt("quantity"));
				UserDocument.setDescription(rs.getString("description"));
				UserDocumentList.add(UserDocument);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return UserDocumentList;
	}
}


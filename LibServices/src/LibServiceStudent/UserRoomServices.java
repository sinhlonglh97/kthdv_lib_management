package LibServiceStudent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import Model.UserDocument;
import Model.UserRoom;

@Path("/userRoomServices")
public class UserRoomServices {
	private static Connection conn = DBConnection.Connect.getConneted(); 
	
	
	@POST
	@Path("/userRoom/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserRoom> getUserRoom() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<UserRoom> UserRoomList = new ArrayList<UserRoom>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT user_room.*,user.name as userName,room.name as roomName"
					+ " FROM user_room"
					+ " inner join user on user_room.userID = user.userID"
					+ " inner join room on user_room.roomID = room.roomID");
			while (rs.next())
			{
				UserRoom UserRoom = new UserRoom();
				UserRoom.setUserID(rs.getInt("userID"));
				UserRoom.setRoomID(rs.getInt("roomID"));
				UserRoom.setUserName(rs.getString("userName"));
				UserRoom.setRoomName(rs.getString("roomName"));
				UserRoom.setDate(rs.getString("date"));
				UserRoom.setStart(rs.getString("startTime"));
				UserRoom.setEnd(rs.getString("endTime"));
				UserRoom.setDescription(rs.getString("description"));
				UserRoomList.add(UserRoom);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return UserRoomList;
	}
	@POST
	@Path("/userRoomWait/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserRoom> getUserRoomWait() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<UserRoom> UserRoomList = new ArrayList<UserRoom>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT user_room_wait.*,user.name as userName,room.name as roomName"
					+ " FROM user_room_wait"
					+ " inner join user on user_room_wait.userID = user.userID"
					+ " inner join room on user_room_wait.roomID = room.roomID");
			while (rs.next())
			{
				UserRoom UserRoom = new UserRoom();
				UserRoom.setUserID(rs.getInt("userID"));
				UserRoom.setRoomID(rs.getInt("roomID"));
				UserRoom.setUserName(rs.getString("userName"));
				UserRoom.setRoomName(rs.getString("roomName"));
				UserRoom.setDate(rs.getString("date"));
				UserRoom.setStart(rs.getString("startTime"));
				UserRoom.setEnd(rs.getString("endTime"));
				UserRoom.setDescription(rs.getString("description"));
				UserRoomList.add(UserRoom);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return UserRoomList;
	}
	
	@POST
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean update(@FormParam("id") int id, @FormParam("father") String father,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "update user_Room set UserRoomID = ?,father = ?,status = ?, description = ? where UserRoomID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, father);
		      preparedStmt.setString(3, status);
		      preparedStmt.setString(4, description);
		      preparedStmt.setInt(5, id);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
	
	@POST
	@Path("/returnRoom/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserRoom> returnRoom(@FormParam("userID") int userID, @FormParam("roomID") String roomID
			)
	{
		
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "Delete from user_room where userID = ? and roomID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, userID);
		      preparedStmt.setString(2, roomID);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		ArrayList<UserRoom> userRoom = getUserRoom();
		return userRoom;
	}
	
	@POST
	@Path("/insert/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean insert(@FormParam("id") int id, @FormParam("father") String father,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		System.out.println("in");
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "insert into UserRoom(UserRoomID,father,status,description) values (?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, father);
		      preparedStmt.setString(3, status);
		      preparedStmt.setString(4, description);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
}


package LibServiceStudent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import Model.Producer;

@Path("/ProducerServices")
public class ProducerServices {
	private static Connection conn = DBConnection.Connect.getConneted(); 
	
	
	@POST
	@Path("/producer/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Producer> getProducer() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Producer> ProducerList = new ArrayList<Producer>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM producer");
			while (rs.next())
			{
				Producer producer = new Producer();
				producer.setProducerID(rs.getInt("producerID"));
				producer.setProducerName(rs.getString("name"));
				producer.setProducerDes(rs.getString("description"));
				producer.setProducerStatus(rs.getString("status"));
				ProducerList.add(producer);
				System.out.println(producer.getProducerDes());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return ProducerList;
	}
	@POST
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean update(@FormParam("id") int id, @FormParam("name") String name,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "update producer set producerID = ?,name = ?,status = ?, description = ? where producerID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, name);
		      preparedStmt.setString(3, status);
		      preparedStmt.setString(4, description);
		      preparedStmt.setInt(5, id);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
	
	@POST
	@Path("/insert/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean insert(@FormParam("id") int id, @FormParam("name") String name,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		System.out.println("in");
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "insert into producer(producerID,name,status,description) values (?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, name);
		      preparedStmt.setString(3, status);
		      
		      preparedStmt.setString(4, description);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
}

package LibServiceStudent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import Model.Author;
import Model.Class;
import Model.Faculty;
import Model.Language;
import Model.Room;
import Model.User;

@Path("/BookServices")
public class BookServices {
	private static Connection conn = DBConnection.Connect.getConneted(); 
	@GET
	@Path("/hello-world/")
	@Produces(MediaType.TEXT_XML)
	public String hello() {
		if (conn != null) {
			return "<?xml version='1.0'?>" + "<hello>connected!</hello>";
		}else
			return "<?xml version='1.0'?>" + "<hello>disconnected!</hello>";
	}
	
	@POST
	@Path("/author/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Author> getAuthors() {
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Author> authorList = new ArrayList<Author>();
		try {
			System.out.println("getAuthors");
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM author");
			while (rs.next())
			{
				Author author = new Author();
				author.setAuthorID(rs.getInt("authorID"));
				author.setAuthorName(rs.getString("name"));
				author.setAuthorDescription(rs.getString("description"));
				authorList.add(author);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return authorList;
	}
	
	@POST
	@Path("/editAuthor/{id}/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Author getAuthor(@PathParam("id") int id) {
		Statement stmt = null;
		ResultSet rs = null;
		Author author = new Author();
		try {
			System.out.println("editAuthor id = "+id);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM author where authorID = " + id);
			while (rs.next())
			{
				
				author.setAuthorID(rs.getInt("authorID"));
				author.setAuthorName(rs.getString("name"));
				author.setAuthorDescription(rs.getString("description"));
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return author;
	}
	
	@POST
	@Path("/updateAuthor/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Author updateAuthor(@FormParam("id") int id,@FormParam("name") String name, @FormParam("description") String description) {
		Statement stmt = null;
		ResultSet rs = null;
		Author author = new Author();
		try {
			   System.out.println("update Author");
			   String query = "update author set name = ?, description =?  where authorID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString(1,name);
		      preparedStmt.setString(2, description);
		      preparedStmt.setInt(3,id);
		      preparedStmt.executeUpdate();
		   
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return author;
	}
	
	
	@POST
	@Path("/addAuthor/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean addAuthor(@FormParam("name") String name, @FormParam("description") String description)
	{
		Statement stmt = null;
		ResultSet rs = null;
		try {
			int maxID = 0;
			System.out.println("insert author");
			String query = " insert into author (authorID ,name, description) values (?, ?, ?)";
		    PreparedStatement preparedStmt = conn.prepareStatement(query);
		    stmt = conn.createStatement();
	      
	      rs = stmt.executeQuery("SELECT * FROM author where authorID = (SELECT MAX(authorID) FROM author)");
	      if(rs.next())
    	  {
    		  if(!rs.wasNull())
    			  maxID = rs.getInt("authorID");
    	  }
	 
	
	      preparedStmt.setInt(1,maxID+1);
	      preparedStmt.setString(2, name);
	      preparedStmt.setString(3, description);
	      preparedStmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

		return true;
	}
	
	@POST
	@Path("/getLanguages/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Language> getLanguages() {
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Language> languageList = new ArrayList<Language>();
		try {
			System.out.println("getLanguages");
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM language");
			while (rs.next())
			{
				Language language = new Language();
				language.setLanguageID(rs.getInt("languageID"));
				language.setName(rs.getString("name"));
				language.setDescription(rs.getString("description"));
				languageList.add(language);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return languageList;
	}
	@POST
	@Path("/addLanguage/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean addLanguage(@FormParam("name") String name, @FormParam("description") String description)
	{
		Statement stmt = null;
		ResultSet rs = null;
		try {
			int maxID = 0;
			System.out.println("insert language");
			String query = " insert into language (languageID ,name, description) values (?, ?, ?)";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      stmt = conn.createStatement();
	      rs = stmt.executeQuery("SELECT * FROM language where languageID = (SELECT MAX(languageID) FROM language)");
	      if(rs.next())
    	  {
    		  if(!rs.wasNull())
    			  maxID = rs.getInt("languageID");
    	  }
	      preparedStmt.setInt(1, maxID+1);
	      preparedStmt.setString(2, name);
	      preparedStmt.setString(3, description);
	      preparedStmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

		return true;
	}
	@POST
	@Path("/updateLanguage/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public void updateLanguage(@FormParam("id") int id,@FormParam("name") String name, @FormParam("description") String description) {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			   System.out.println("update Language");
			   String query = "update language set name = ?, description =?  where languageID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString(1,name);
		      preparedStmt.setString(2, description);
		      preparedStmt.setInt(3,id);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	@POST
	@Path("/editLanguage/{id}/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Language getLanguage(@PathParam("id") int id) {
		Statement stmt = null;
		ResultSet rs = null;
		Language language = new Language();
		try {
			System.out.println("editLanguage id = "+id);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM language where languageID = " + id);
			while (rs.next())
			{
				language.setLanguageID(rs.getInt("languageID"));
				language.setName(rs.getString("name"));
				language.setDescription(rs.getString("description"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return language;
	}
	@POST
	@Path("/sign-in-secure-v2/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public User signIn_ver2(@FormParam("username") String username, @FormParam("password") String password)
	{
		
		Statement stmt = null;
		ResultSet rs = null;
		User user = new User();
		try {
			System.out.println("login");
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM user");
			while (rs.next())
			{
				String u = rs.getString("username");
				String p = rs.getString("password");
				if(username.equals(u) && (password).equals(p))
				{
					System.out.println("Login success!");
					user.setId(rs.getInt("userID"));
					user.setEmail(rs.getString("email"));
					user.setPhone(rs.getString("phone"));
					user.setClassID(rs.getInt("classID"));
					user.setFacultyID(rs.getInt("facultyID"));
					user.setName(rs.getString("name"));
					user.setBirthday(rs.getString("birthday"));
					user.setAddress(rs.getString("address"));
					user.setFunction(rs.getString("function"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return user;
	}
	@POST
	@Path("/getRoomCategories/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<RoomCategory> getRoomCategories() {
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<RoomCategory> list = new ArrayList<RoomCategory>();
		try {
			System.out.println("getRoomCategories");
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM roomcategory");
			while (rs.next())
			{
				RoomCategory rc = new RoomCategory();
				rc.setId(rs.getInt("roomCategoryID"));
				rc.setName(rs.getString("name"));
				rc.setDescription(rs.getString("description"));
				list.add(rc);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return list;
	}
	
	@POST
	@Path("/addRoomCategory/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean addRoomCategory(@FormParam("name") String name, @FormParam("description") String description)
	{
		Statement stmt = null;
		ResultSet rs = null;
		try {
			int maxID = 0;
			System.out.println("insert roomCategory");
			String query = " insert into roomcategory (roomCategoryID ,name, description) values (?, ?, ?)";
		    PreparedStatement preparedStmt = conn.prepareStatement(query);
		    stmt = conn.createStatement();
	      
		    rs = stmt.executeQuery("SELECT * FROM roomcategory where roomCategoryID = (SELECT MAX(roomCategoryID) FROM roomcategory)");
	      
	
	    	  if(rs.next())
	    	  {
	    		  if(!rs.wasNull())
	    			  maxID = rs.getInt("roomCategoryID");
	    	  }
	    	  
	    	  
	    		  
	    	  
	
	      preparedStmt.setInt(1,maxID+1);
	      preparedStmt.setString(2, name);
	      preparedStmt.setString(3, description);
	      preparedStmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

		return true;
	}
	
	@POST
	@Path("/editRoomCategory/{id}/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public RoomCategory getRoomCategory(@PathParam("id") int id) {
		Statement stmt = null;
		ResultSet rs = null;
		RoomCategory roomCategory = new RoomCategory();
		try {
			System.out.println("editRoomCategory id = "+id);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM roomcategory where roomCategoryID = " + id);
			while (rs.next())
			{
				
				roomCategory.setId(rs.getInt("roomCategoryID"));
				roomCategory.setName(rs.getString("name"));
				roomCategory.setDescription(rs.getString("description"));
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return roomCategory;
	}
	
	@POST
	@Path("/updateRoomCategory/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public void updateRoomCategory(@FormParam("id") int id,@FormParam("name") String name, @FormParam("description") String description) {
		Statement stmt = null;
		ResultSet rs = null;
	
		try {
			   System.out.println("update RoomCategory");
			   String query = "update roomcategory set name = ?, description =?  where roomCategoryID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString(1,name);
		      preparedStmt.setString(2, description);
		      preparedStmt.setInt(3,id);
		      preparedStmt.executeUpdate();
		   
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

	}
	
	@POST
	@Path("/addRoom/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean addRoom(@FormParam("name") String name, @FormParam("description") String description,
			@FormParam("roomC") int roomC,@FormParam("status") String status)
	{
		Statement stmt = null;
		ResultSet rs = null;
		try {
			int maxID = 0;
			System.out.println("insert room");
			String query = " insert into room (roomID ,name,  roomCategoryID, status, description) values (?, ?, ?, ?, ?)";
		    PreparedStatement preparedStmt = conn.prepareStatement(query);
		    stmt = conn.createStatement();
	      
		    rs = stmt.executeQuery("SELECT * FROM room where roomID = (SELECT MAX(roomID) FROM room)");
	      
	
	    	  if(rs.next())
	    	  {
	    		  if(!rs.wasNull())
	    			  maxID = rs.getInt("roomID");
	    	  }
	    	  
	    	  
	    		  
	    	  
	
	      preparedStmt.setInt(1,maxID+1);
	      preparedStmt.setString(2, name);
	      preparedStmt.setInt(3,roomC);
	      preparedStmt.setString(4, status);
	      preparedStmt.setString(5, description);
	      preparedStmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

		return true;
	}
	
	@POST
	@Path("/addFaculty/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean addFaculty(@FormParam("name") String name, @FormParam("description") String description)
	{
		Statement stmt = null;
		ResultSet rs = null;
		try {
			int maxID = 0;
			System.out.println("insert faculty");
			String query = " insert into faculty (facultyID ,name, description) values (?, ?, ?)";
		    PreparedStatement preparedStmt = conn.prepareStatement(query);
		    stmt = conn.createStatement();
	      
	      rs = stmt.executeQuery("SELECT * FROM faculty where facultyID = (SELECT MAX(facultyID) FROM faculty)");
	      if(rs.next())
    	  {
    		  if(!rs.wasNull())
    			  maxID = rs.getInt("facultyID");
    	  }
	 
	
	      preparedStmt.setInt(1,maxID+1);
	      preparedStmt.setString(2, name);
	      preparedStmt.setString(3, description);
	      preparedStmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

		return true;
	}
	
	@POST
	@Path("/addClass/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean addClass(@FormParam("name") int name, @FormParam("fID") int fID)
	{
		Statement stmt = null;
		ResultSet rs = null;
		try {
			
			System.out.println("insert class");
			String query = " insert into class (classID , facultyID) values (?, ?)";
		    PreparedStatement preparedStmt = conn.prepareStatement(query);
		   
		      preparedStmt.setInt(1,name);
		      preparedStmt.setInt(2, fID);
	
		      preparedStmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

		return true;
	}
	
	
	

	@POST
	@Path("/getFaculties/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Faculty> getFaculties() {
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Faculty> list = new ArrayList<Faculty>();
		try {
			System.out.println("getFaculties");
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM faculty");
			while (rs.next())
			{
				Faculty f = new Faculty();
				f.setId(rs.getInt("facultyID"));
				f.setName(rs.getString("name"));
				f.setDescription(rs.getString("description"));
				list.add(f);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return list;
	}
	

	@POST
	@Path("/getClasses/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Class> getClasses() {
		Statement stmt = null;
		ResultSet rs = null;
		Statement stmt2 = null;
		ResultSet rs2 = null;
		ArrayList<Class> list = new ArrayList<Class>();
		try {
			System.out.println("getClasses");
			stmt = conn.createStatement();
			stmt2 = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM class");
			while (rs.next())
			{
				String faculty = "";
				int fID = rs.getInt("facultyID");
				rs2 = stmt2.executeQuery("SELECT * FROM faculty where facultyID = "+fID);
				if(rs2.next())
				{
					faculty = rs2.getString("name");
				}
				Class f = new Class();
				f.setId(rs.getInt("classID"));
				f.setIdFaculty(rs.getInt("facultyID"));
				f.setFaculty(faculty);
				list.add(f);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (rs2 != null) rs2.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt2 != null) stmt2.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return list;
	}
	

	@POST
	@Path("/getRooms/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Room> getRooms() {
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		Statement stmt2 = null;
		ArrayList<Room> list = new ArrayList<Room>();
		try {
			System.out.println("getRooms");
			stmt = conn.createStatement();
			stmt2 = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM room");
			while (rs.next())
			{
				int Cid = rs.getInt("roomCategoryID");
//				System.out.println(Cid);
				String category = "";
				rs2 = stmt2.executeQuery("SELECT * from roomcategory where roomCategoryID = "+Cid);
				rs2.next();
				category = rs2.getString("name");
				Room f = new Room();
				f.setId(rs.getInt("roomID"));
				f.setName(rs.getString("name"));
				f.setStatus(rs.getString("status"));
				f.setDescription(rs.getString("description"));
				f.setCid(rs.getInt("roomCategoryID"));
				f.setCategory(category);
				list.add(f);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (rs2 != null) rs2.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt2 != null) stmt2.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return list;
	}
	
	@POST
	@Path("/editFaculty/{id}/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Faculty getFaculty(@PathParam("id") int id) {
		Statement stmt = null;
		ResultSet rs = null;
		Faculty  r = new Faculty();
		try {
			System.out.println("editFaculty id = "+id);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM faculty where facultyID = " + id);
			while (rs.next())
			{
				
				r.setId(rs.getInt("facultyID"));
				r.setName(rs.getString("name"));
				r.setDescription(rs.getString("description"));
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return r;
	}
	
	@POST
	@Path("/updateFaculty/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public void updateFaculty(@FormParam("id") int id,@FormParam("name") String name, @FormParam("description") String description) {
		Statement stmt = null;
		ResultSet rs = null;
	
		try {
			   System.out.println("update Faculty");
			   String query = "update faculty set name = ?, description =?  where facultyID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString(1,name);
		      preparedStmt.setString(2, description);
		      preparedStmt.setInt(3,id);
		      preparedStmt.executeUpdate();
		   
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

	}
	
	@POST
	@Path("/editClass/{id}/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Class getClass(@PathParam("id") int id) {
		Statement stmt = null;
		ResultSet rs = null;
		Class  r = new Class();
		try {
			System.out.println("editClass id = "+id);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM class where classID = " + id);
			while (rs.next())
			{
				
				r.setId(rs.getInt("classID"));
				r.setIdFaculty(rs.getInt("facultyID"));
				
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return r;
	}
	
	@POST
	@Path("/updateClass/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public void updateClass(@FormParam("name") int name, @FormParam("fID") int fID) {
		Statement stmt = null;
		ResultSet rs = null;
	
		try {
			   System.out.println("update Class");
			   String query = "update class set classID = ?, facultyID =?  where classID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1,name);
		      preparedStmt.setInt(2, fID);
		      preparedStmt.setInt(3,name);
		      preparedStmt.executeUpdate();
		   
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

	}
	
	@POST
	@Path("/editRoom/{id}/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Room getRoom(@PathParam("id") int id) {
		Statement stmt = null;
		ResultSet rs = null;
		Room  r = new Room();
		try {
			System.out.println("editRoom id = "+id);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM room where roomID = " + id);
			while (rs.next())
			{
				
				r.setId(rs.getInt("roomID"));
				r.setCid(rs.getInt("roomCategoryID"));
				r.setName(rs.getString("name"));
				r.setDescription(rs.getString("description"));
				r.setStatus(rs.getString("status"));
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return r;
	}
	
	@POST
	@Path("/updateRoom/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public void updateRoom(@FormParam("id") int id,@FormParam("name") String name,@FormParam("cID") int cID, @FormParam("description") String description,@FormParam("status") String status) {
		Statement stmt = null;
		ResultSet rs = null;
	
		try {
			   System.out.println("update Room");
			   String query = "update room set name = ?, roomCategoryID = ? ,  description = ?, status =?  where roomID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString(1,name);
		      preparedStmt.setInt(2,cID);
		      preparedStmt.setString(3, description);
		      preparedStmt.setString(4,status);
		      preparedStmt.setInt(5,id);
		      preparedStmt.executeUpdate();
		   
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

	}
	
	@POST
	@Path("/addUser/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)

	public boolean addUser(	@FormParam("id") int id, @FormParam("name") String name,
			@FormParam("faculty") int faculty, @FormParam("class") int cl,
			@FormParam("username") String username, @FormParam("password") String password,
			@FormParam("email") String email,@FormParam("role") String role)
	{
		Statement stmt = null;
		ResultSet rs = null;
		try {
	
			System.out.println("insert user");
			String query = " insert into user (userID ,name, facultyID, classID, username, password , email,phone ,address,birthday,function  ) values (?, ?, ?,?, ?, ?,?,?,?,?,?)";
		    PreparedStatement preparedStmt = conn.prepareStatement(query);	
		      preparedStmt.setInt(1,id);
		      preparedStmt.setString(2, name);
		      preparedStmt.setInt(3, faculty);
		      preparedStmt.setInt(4,cl);
		      preparedStmt.setString(5, name);
		      preparedStmt.setString(6, password.hashCode()+"");
		      preparedStmt.setString(7,email);
		      preparedStmt.setString(8,"");
		      preparedStmt.setString(9, "");
		      preparedStmt.setString(10,"");
		      preparedStmt.setString(11,role);
		      preparedStmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}

		return true;
	}
	
	
	@POST
	@Path("/editUser/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public User editUser(@FormParam("id") int id,@FormParam("name") String name,@FormParam("email") String email, @FormParam("phone") String phone,@FormParam("birthday") String birthday,@FormParam("address") String address) {
		Statement stmt = null;
		ResultSet rs = null;
	
		try {
			   System.out.println("update User");
			 User user = new User(); 
			   String query = "update user set name = ?, phone = ?, birthday = ?, address = ?  where userID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString(1,name);
		      preparedStmt.setString(2, phone);
		      preparedStmt.setString(3,birthday);
		      preparedStmt.setString(4, address);
		      preparedStmt.setInt(5,id);
		      preparedStmt.executeUpdate();
		      
		      user.setId(id);
		      user.setPhone(phone);
		      user.setBirthday(birthday);
		      user.setName(name);
		      user.setAddress(address);
		      user.setEmail(email);
		      
		      return user;
		   
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return null;
	}
	
	@POST
	@Path("/getUsers/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> getUsers() {
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<User> list = new ArrayList<User>();
		try {
			System.out.println("getUsers");
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM user");
			while (rs.next())
			{
				User user = new User();
				user.setId(rs.getInt("userID"));
				user.setName(rs.getString("name"));
				user.setPassword(rs.getString("password"));
				user.setEmail(rs.getString("email"));
				user.setPhone(rs.getString("phone"));
				user.setBirthday(rs.getString("birthday"));
				user.setAddress(rs.getString("address"));
				list.add(user);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return list;
	}
	
}

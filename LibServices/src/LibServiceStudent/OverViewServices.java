package LibServiceStudent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import Model.OverView;

@Path("/OverViewServices")
public class OverViewServices {
	private static Connection conn = DBConnection.Connect.getConneted(); 
	
	
	@POST
	@Path("/OverView/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public OverView getOverView() {
		
		Statement stmt = null;
		ResultSet rs = null;
		OverView overview = new OverView();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select COUNT(*) from user");
			rs.next();
			overview.setCountUser(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from document");
			rs.next();
			overview.setCountDoucument(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from room");
			rs.next();
			overview.setCountRoom(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from computer");
			rs.next();
			overview.setCountComputer(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from user_document");
			rs.next();
			overview.setCountUserDocument(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from user_room");
			rs.next();
			overview.setCountUserRoom(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from user_computer");
			rs.next();
			overview.setCountUserComputer(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from computercategory");
			rs.next();
			overview.setCountComputerType(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from category");
			rs.next();
			overview.setCountCategory(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from nxb");
			rs.next();
			overview.setCountNxb(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from producer");
			rs.next();
			overview.setCountPlacer(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from author");
			rs.next();
			overview.setCountAuthor(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from place");
			rs.next();
			overview.setCountPlace(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from roomcategory");
			rs.next();
			overview.setCountRoomTypr(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from class");
			rs.next();
			overview.setCountClass(rs.getInt(1));
			
			rs = stmt.executeQuery("select COUNT(*) from faculty");
			rs.next();
			overview.setCountFaculty(rs.getInt(1));
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return overview;
	}
}


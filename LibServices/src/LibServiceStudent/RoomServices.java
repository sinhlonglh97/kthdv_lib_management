package LibServiceStudent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import Model.Room;
import Model.UserRoom;

@Path("/RoomServices")
public class RoomServices {
	private static Connection conn = DBConnection.Connect.getConneted(); 
	@POST
	@Path("/getRoomEmpty/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Room> getRooms() {
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		Statement stmt2 = null;
		ArrayList<Room> list = new ArrayList<Room>();
		try {
			System.out.println("getRooms");
			stmt = conn.createStatement();
			stmt2 = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM room WHERE room.roomID not in(select roomID from user_room)");
			while (rs.next())
			{
				int Cid = rs.getInt("roomCategoryID");
				String category = "";
				rs2 = stmt2.executeQuery("SELECT * from roomcategory where roomCategoryID = "+Cid);
				rs2.next();
				category = rs2.getString("name");
				Room f = new Room();
				f.setId(rs.getInt("roomID"));
				f.setName(rs.getString("name"));
				f.setStatus(rs.getString("status"));
				f.setDescription(rs.getString("description"));
				f.setCid(rs.getInt("roomCategoryID"));
				f.setCategory(category);
				list.add(f);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (rs2 != null) rs2.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt2 != null) stmt2.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return list;
	}
	
	@POST
	@Path("/search/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Room> search(@FormParam("value") String value,@FormParam("option") String option) {
		ArrayList<Room> list = new ArrayList<Room>();
		if(option.equals("name")) {
			list = searchByName(value);
		}else
		{
			list = searchByNameAndCategory(value,option);
		}
		return list;
	}
	
	public ArrayList<Room> searchByName(String value){
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		Statement stmt2 = null;
		ArrayList<Room> list = new ArrayList<Room>();
		try {
			System.out.println("getRooms");
			stmt = conn.createStatement();
			stmt2 = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM room WHERE room.roomID not in(select roomID from user_room) and name like '%" + value + "%'");
			while (rs.next())
			{
				int Cid = rs.getInt("roomCategoryID");
				String category = "";
				rs2 = stmt2.executeQuery("SELECT * from roomcategory where roomCategoryID = "+Cid);
				rs2.next();
				category = rs2.getString("name");
				Room f = new Room();
				f.setId(rs.getInt("roomID"));
				f.setName(rs.getString("name"));
				f.setStatus(rs.getString("status"));
				f.setDescription(rs.getString("description"));
				f.setCid(rs.getInt("roomCategoryID"));
				f.setCategory(category);
				list.add(f);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (rs2 != null) rs2.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt2 != null) stmt2.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return list;
	}
	public ArrayList<Room> searchByNameAndCategory(String value,String option){
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		Statement stmt2 = null;
		ArrayList<Room> list = new ArrayList<Room>();
		try {
			System.out.println("getRooms");
			stmt = conn.createStatement();
			stmt2 = conn.createStatement();
			System.out.println(option);
			rs = stmt.executeQuery("SELECT * FROM room WHERE room.roomID not in(select roomID from user_room) and name like '%" + value + "%'"
					+ " and room.roomCategoryID in (select roomCategoryID from roomcategory where name like '"+option+"')");
			while (rs.next())
			{
				int Cid = rs.getInt("roomCategoryID");
				String category = "";
				rs2 = stmt2.executeQuery("SELECT * from roomcategory where roomCategoryID = "+Cid);
				rs2.next();
				category = rs2.getString("name");
				Room f = new Room();
				f.setId(rs.getInt("roomID"));
				f.setName(rs.getString("name"));
				f.setStatus(rs.getString("status"));
				f.setDescription(rs.getString("description"));
				f.setCid(rs.getInt("roomCategoryID"));
				f.setCategory(category);
				list.add(f);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (rs2 != null) rs2.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt2 != null) stmt2.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return list;
	}
	@POST
	@Path("/borrow/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean borrow(@FormParam("roomID") int roomID, @FormParam("userID") int userID,
			@FormParam("timeStart") String start,@FormParam("timeEnd") String end,
			@FormParam("description") String description,@FormParam("date") String date)
	{
		
		System.out.println("in");
		System.out.println(roomID);
		System.out.println(userID);
		System.out.println(start);
		System.out.println(end);
		System.out.println(date);
		System.out.println(description);
		
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "insert into user_room_wait(roomID,userID,startTime,endTime,date,description) values (?,?,?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, roomID);
		      preparedStmt.setInt(2, userID);
		      preparedStmt.setString(3, start);
		      preparedStmt.setString(4, end);
		      preparedStmt.setString(5, date);
		      preparedStmt.setString(6, description);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
	@POST
	@Path("/deny/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserRoom> deny(@FormParam("roomID") int roomID, @FormParam("userID") int userID,
			@FormParam("timeStart") String start,@FormParam("timeEnd") String end,
			@FormParam("description") String description,@FormParam("date") String date)
	{
		
		System.out.println("deny");
		System.out.println(roomID);
		System.out.println(userID);
		System.out.println(start);
		System.out.println(end);
		System.out.println(date);
		System.out.println(description);
		Statement stmt = null;
		ResultSet rs = null;
		try {
		     String query2 = "delete from user_room_wait where roomID = ? and userID = ?";
		     PreparedStatement preparedStmt2 = conn.prepareStatement(query2);
		      preparedStmt2.setInt(1, roomID);
		      preparedStmt2.setInt(2, userID);
		      preparedStmt2.executeUpdate();
		     
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return getUserRoomWait();
	}
		public ArrayList<UserRoom> getUserRoomWait() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<UserRoom> UserRoomList = new ArrayList<UserRoom>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT user_room_wait.*,user.name as userName,room.name as roomName"
					+ " FROM user_room_wait"
					+ " inner join user on user_room_wait.userID = user.userID"
					+ " inner join room on user_room_wait.roomID = room.roomID");
			while (rs.next())
			{
				UserRoom UserRoom = new UserRoom();
				UserRoom.setUserID(rs.getInt("userID"));
				UserRoom.setRoomID(rs.getInt("roomID"));
				UserRoom.setUserName(rs.getString("userName"));
				UserRoom.setRoomName(rs.getString("roomName"));
				UserRoom.setDate(rs.getString("date"));
				UserRoom.setStart(rs.getString("startTime"));
				UserRoom.setEnd(rs.getString("endTime"));
				UserRoom.setDescription(rs.getString("description"));
				UserRoomList.add(UserRoom);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return UserRoomList;
	}
	public ArrayList<UserRoom> getUserRoom() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<UserRoom> UserRoomList = new ArrayList<UserRoom>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT user_room.*,user.name as userName,room.name as roomName"
					+ " FROM user_room"
					+ " inner join user on user_room.userID = user.userID"
					+ " inner join room on user_room.roomID = room.roomID");
			while (rs.next())
			{
				UserRoom UserRoom = new UserRoom();
				UserRoom.setUserID(rs.getInt("userID"));
				UserRoom.setRoomID(rs.getInt("roomID"));
				UserRoom.setUserName(rs.getString("userName"));
				UserRoom.setRoomName(rs.getString("roomName"));
				UserRoom.setDate(rs.getString("date"));
				UserRoom.setStart(rs.getString("startTime"));
				UserRoom.setEnd(rs.getString("endTime"));
				UserRoom.setDescription(rs.getString("description"));
				UserRoomList.add(UserRoom);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return UserRoomList;
	}
	@POST
	@Path("/accept/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserRoom> accept(@FormParam("roomID") int roomID, @FormParam("userID") int userID,
			@FormParam("timeStart") String start,@FormParam("timeEnd") String end,
			@FormParam("description") String description,@FormParam("date") String date)
	{
		
		System.out.println("accept");
		System.out.println(roomID);
		System.out.println(userID);
		System.out.println(start);
		System.out.println(end);
		System.out.println(date);
		System.out.println(description);
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		String room = null;
		String email = null;
		try {
			String query = "insert into user_room(roomID,userID,startTime,endTime,date,description) values (?,?,?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, roomID);
		      preparedStmt.setInt(2, userID);
		      preparedStmt.setString(3, start);
		      preparedStmt.setString(4, end);
		      preparedStmt.setString(5, date);
		      preparedStmt.setString(6, description);
		      preparedStmt.executeUpdate();
		     String query2 = "delete from user_room_wait where roomID = ? and userID = ?";
		     PreparedStatement preparedStmt2 = conn.prepareStatement(query2);
		      preparedStmt2.setInt(1, roomID);
		      preparedStmt2.setInt(2, userID);
		      preparedStmt2.executeUpdate();
		      stmt = conn.createStatement();
				rs = stmt.executeQuery(
						"SELECT name from room where roomID = "+ roomID);
				while (rs.next())
				{
					room = rs.getString("name");
				}
				rs2 = stmt.executeQuery(
						"SELECT email from user where userID = "+ userID);
				while (rs2.next())
				{
					email = rs2.getString("email");
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (rs2 != null) rs2.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		emailBorrowRoom(email, room, start, end);
		return getUserRoomWait();
	}
	private void emailBorrowRoom(String email, String room, String start, String end)
	{
		final String username = "noreply.jsv@gmail.com";
		final String password = "6tfcvbnm";
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply.jsv@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject("Borrow room is successful");
			message.setText("Dear " + email + ","
					+ "\n\nYou have successfully booked room " + room
					+ "\n\nYou can use this room from "+ start +" to "+ end+","
					+ "\n\nThank you for using the library's services"
					+ "\n\nRegards,"
					+ "\nLibrarySystem");
			Transport.send(message);
			System.out.println("Done");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
}

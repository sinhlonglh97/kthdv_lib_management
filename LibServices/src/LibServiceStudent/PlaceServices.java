package LibServiceStudent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import Model.Place;

@Path("/PlaceServices")
public class PlaceServices {
	private static Connection conn = DBConnection.Connect.getConneted(); 
	
	
	@POST
	@Path("/place/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Place> getplace() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Place> placeList = new ArrayList<Place>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM place");
			while (rs.next())
			{
				Place place = new Place();
				place.setPlaceID(rs.getInt("placeID"));
				place.setPlaceFather(rs.getString("father"));
				place.setPlaceDes(rs.getString("description"));
				place.setPlaceStatus(rs.getString("status"));
				placeList.add(place);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return placeList;
	}
	@POST
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean update(@FormParam("id") int id, @FormParam("father") String father,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "update place set placeID = ?,father = ?,status = ?, description = ? where placeID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, father);
		      preparedStmt.setString(3, status);
		      preparedStmt.setString(4, description);
		      preparedStmt.setInt(5, id);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
	
	@POST
	@Path("/insert/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean insert(@FormParam("id") int id, @FormParam("father") String father,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		System.out.println("in");
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "insert into place(placeID,father,status,description) values (?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, father);
		      preparedStmt.setString(3, status);
		      preparedStmt.setString(4, description);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
}

package LibServiceStudent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import Model.UserDocument;

@Path("/userDocumentServices")
public class UserDocumentServices {
	private static Connection conn = DBConnection.Connect.getConneted(); 
	
	
	@POST
	@Path("/userDocument/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserDocument> getUserDocument() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<UserDocument> UserDocumentList = new ArrayList<UserDocument>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT user_document.*,user.name as userName,document.name as documentName"
					+ " FROM user_document"
					+ " inner join user on user_document.userID = user.userID"
					+ " inner join document on user_document.documentID = document.documentID");
			while (rs.next())
			{
				UserDocument UserDocument = new UserDocument();
				UserDocument.setUserID(rs.getInt("userID"));
				UserDocument.setDocumentID(rs.getInt("documentID"));
				UserDocument.setUserName(rs.getString("userName"));
				UserDocument.setDocumentName(rs.getString("documentName"));
				UserDocument.setStartDate(rs.getString("startDate"));
				UserDocument.setEndDate(rs.getString("endDate"));
				UserDocument.setQuantity(rs.getInt("quantity"));
				UserDocument.setDescription(rs.getString("description"));
				UserDocumentList.add(UserDocument);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return UserDocumentList;
	}
	@POST
	@Path("/userDocumentWait/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserDocument> getUserDocumentWait() {
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<UserDocument> UserDocumentList = new ArrayList<UserDocument>();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT user_document_wait.*,user.name as userName,document.name as documentName"
					+ " FROM user_document_wait"
					+ " inner join user on user_document_wait.userID = user.userID"
					+ " inner join document on user_document_wait.documentID = document.documentID");
			while (rs.next())
			{
				UserDocument UserDocument = new UserDocument();
				UserDocument.setUserID(rs.getInt("userID"));
				UserDocument.setDocumentID(rs.getInt("documentID"));
				UserDocument.setUserName(rs.getString("userName"));
				UserDocument.setDocumentName(rs.getString("documentName"));
				UserDocument.setStartDate(rs.getString("startDate"));
				UserDocument.setEndDate(rs.getString("endDate"));
				UserDocument.setQuantity(rs.getInt("quantity"));
				UserDocument.setDescription(rs.getString("description"));
				UserDocumentList.add(UserDocument);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return UserDocumentList;
	}
	@POST
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean update(@FormParam("id") int id, @FormParam("father") String father,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "update user_document set UserDocumentID = ?,father = ?,status = ?, description = ? where UserDocumentID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, father);
		      preparedStmt.setString(3, status);
		      preparedStmt.setString(4, description);
		      preparedStmt.setInt(5, id);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
	
	@POST
	@Path("/returnDocument/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UserDocument> returnDocument(@FormParam("userID") int userID, @FormParam("documentID") String documentID,
			@FormParam("quantity") int quantity)
	{
		
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "Delete from user_document where userID = ? and documentID = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, userID);
		      preparedStmt.setString(2, documentID);
		      preparedStmt.executeUpdate();
		    String query2 = "update document set quantity = quantity + ?";
		      PreparedStatement preparedStmt2 = conn.prepareStatement(query2);
		      preparedStmt2.setInt(1, quantity);
		      preparedStmt2.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		ArrayList<UserDocument> userDocument = getUserDocument();
		return userDocument;
	}
	
	@POST
	@Path("/insert/")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean insert(@FormParam("id") int id, @FormParam("father") String father,
			@FormParam("status") String status,@FormParam("description") String description)
	{
		System.out.println("in");
		boolean result = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "insert into UserDocument(UserDocumentID,father,status,description) values (?,?,?,?)";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setInt(1, id);
		      preparedStmt.setString(2, father);
		      preparedStmt.setString(3, status);
		      preparedStmt.setString(4, description);
		      preparedStmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return result;
	}
}

